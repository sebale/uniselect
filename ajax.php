<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 *
 * @package    report
 * @subpackage univselect
 * @copyright  2015
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

define('AJAX_SCRIPT', true);

require('../../config.php');

$action = optional_param('action', '', PARAM_TEXT);
$groupid = optional_param('groupid', '', PARAM_INT);
$courseid = optional_param('courseid', '', PARAM_INT);

if (!isloggedin() or isguestuser()) {
	return false;
}
require_login();

$PAGE->set_context(context_system::instance());

if($action == 'get_group_students'){
    if($groupid>0){
        $sql = "SELECT u.* FROM mdl_user u, mdl_user_enrolments ue, mdl_enrol e, mdl_role_assignments ra, mdl_context ctx, mdl_groups_members gm WHERE ra.roleid = 5 AND e.courseid = $courseid AND u.id = ue.userid AND e.id = ue.enrolid AND ctx.instanceid = e.courseid AND ra.contextid = ctx.id AND ue.userid = ra.userid AND gm.userid=u.id AND gm.groupid=$groupid GROUP BY e.courseid, ue.userid ORDER BY u.firstname ASC";
    }else{
        $sql = "SELECT u.* FROM mdl_user u, mdl_user_enrolments ue, mdl_enrol e, mdl_role_assignments ra, mdl_context ctx WHERE ra.roleid = 5 AND e.courseid = $courseid AND u.id = ue.userid AND e.id = ue.enrolid AND ctx.instanceid = e.courseid AND ra.contextid = ctx.id AND ue.userid = ra.userid GROUP BY e.courseid, ue.userid ORDER BY u.firstname ASC";
    }
    $users = $DB->get_records_sql($sql);
    $data = array();
    foreach($users as $user){
        $data[$user->id] = fullname($user);
    }

	die(json_encode($data));
}