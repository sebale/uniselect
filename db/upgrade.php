<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * This plugin provides access to Moodle data in form of analytics and reports in real time.
 *
 * @package    report_univselect
 * @copyright  2020 IntelliBoard, Inc
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @website    http://intelliboard.net/
 */

require_once($CFG->dirroot.'/report/univselect/table.php');

function xmldb_report_univselect_upgrade($oldversion) {
    global $DB;

    $dbman = $DB->get_manager();

    if ($oldversion < 2020110301) {
        $table = new xmldb_table('report_univselect_scales');

        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('practice_test_id', XMLDB_TYPE_INTEGER, '11', null, null, null);
        $table->add_field('quiz_type', XMLDB_TYPE_CHAR, '50', null, null, null, null);
        $table->add_field('correct', XMLDB_TYPE_INTEGER, '11', null, null, null);
        $table->add_field('score', XMLDB_TYPE_INTEGER, '11', null, null, null);

        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

        upgrade_plugin_savepoint(true, 2020110301, 'report', 'univselect');
    }

    if ($oldversion < 2020110302) {
        $scales = univselect_get_old_scaled_scores();
        foreach ($scales as $practice_test => $pt_scales) {
            $pt_record = $DB->get_record('tag', array('rawname' => $practice_test));
            $record = new stdClass();
            $record->practice_test_id = $pt_record->id;

            foreach ($pt_scales as $quiz_type => $scale) {
                $record->quiz_type = $quiz_type;
                foreach ($scale as $correct => $score) {
                    $record->correct = $correct;
                    if($original_record = $DB->get_record('report_univselect_scales', (array) $record)){
                        $original_record->score = $score;
                        $DB->update_record('report_univselect_scales', $original_record);
                    } elseif($score > 0) {
                        $record->score = $score;
                        $DB->insert_record('report_univselect_scales', $record);
                    }
                }
            }
        }
        $scales = univselect_get_old_scaled_scores_act();
        foreach ($scales as $practice_test => $pt_scales) {
            $pt_record = $DB->get_record('tag', array('rawname' => $practice_test));
            $record = new stdClass();
            $record->practice_test_id = $pt_record->id;

            foreach ($pt_scales as $quiz_type => $scale) {
                $record->quiz_type = $quiz_type;
                foreach ($scale as $correct => $score) {
                    $record->correct = $correct;
                    if($original_record = $DB->get_record('report_univselect_scales', (array) $record)){
                        $original_record->score = $score;
                        $DB->update_record('report_univselect_scales', $original_record);
                    } elseif($score > 0) {
                        $record->score = $score;
                        $DB->insert_record('report_univselect_scales', $record);
                    }
                }
            }
        }

        upgrade_plugin_savepoint(true, 2020110302, 'report', 'univselect');
    }

    if ($oldversion < 2020113000) {
        $scales = univselect_get_old_scaled_scores_act();
        foreach ($scales as $practice_test => $pt_scales) {
            $pt_record = $DB->get_record('tag', array('rawname' => $practice_test));
            $record = new stdClass();
            $record->practice_test_id = $pt_record->id;

            foreach ($pt_scales as $quiz_type => $scale) {
                $record->quiz_type = $quiz_type;
                foreach ($scale as $correct => $score) {
                    $record->correct = $correct;
                    if($original_record = $DB->get_record('report_univselect_scales', (array) $record)){
                        $original_record->score = $score;
                        $DB->update_record('report_univselect_scales', $original_record);
                    } elseif($score > 0) {
                        $record->score = $score;
                        $DB->insert_record('report_univselect_scales', $record);
                    }
                }
            }
        }

        upgrade_plugin_savepoint(true, 2020113000, 'report', 'univselect');
    }

    return true;
}
