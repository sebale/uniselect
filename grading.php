<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 *
 * @package    report
 * @subpackage univselect
 * @copyright  2015
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require "../../config.php";
require "table.php";
require_once($CFG->libdir.'/adminlib.php');
$quiz = optional_param('quiz', 0, PARAM_INT);
$download = optional_param('download', '', PARAM_ALPHA);
$courseid = optional_param('courseid', 0, PARAM_INT);
$quiz = optional_param('quiz', 0, PARAM_INT);
$length = optional_param('length', 100, PARAM_INT);

require_login();


if (is_siteadmin($USER->id)) {
	$courses = $DB->get_records_sql("SELECT * FROM mdl_course where category > 0 and visible = 1");
}else{
	$courses = $DB->get_records_sql("SELECT c.* FROM mdl_user_enrolments ue, mdl_enrol e, mdl_course c where c.category > 0 and c.visible = 1 AND c.id = e.courseid AND ue.enrolid = e.id GROUP BY c.id");
}

if(!$courseid){
    $courseid = reset($courses)->id;
}

$student = false;
$context = context_course::instance($courseid);
if (!has_capability('report/univselect:see_all_users', $context)) {
	$userid = $USER->id;
	$student = true;
}

$PAGE->requires->jquery();
$PAGE->set_context($context);
$PAGE->set_url('/report/univselect/grading.php', array('courseid'=>$courseid, 'quiz'=>$quiz));


$table = new univselect_table3('table2', $courseid, $quiz);
$table->is_downloading($download, get_string('univselect3', 'report_univselect'), get_string('univselect1', 'report_univselect'));

if (!$table->is_downloading()) {
	if (has_capability('moodle/course:update', $context)) {
		admin_externalpage_setup('reportunivselect3', '', null, '', array('pagelayout'=>'report'));
	}else{
		$PAGE->set_title(get_string('univselect3', 'report_univselect'));
		$PAGE->set_heading(get_string('univselect3', 'report_univselect'));
	}
    echo $OUTPUT->header();
    if (has_capability('moodle/course:update', $context)) {
		echo $OUTPUT->heading(get_string('univselect3', 'report_univselect'));
	}


	echo html_writer::start_tag("form",  array("action"=>$CFG->wwwroot.'/report/univselect/grading.php'));
	echo html_writer::start_tag("label",  array("style"=>" margin: 20px auto;"));
	echo html_writer::tag("span", "Filter: ");

	echo html_writer::start_tag('select', array('name'=>'length'));
	$options = array(10=>10, 20=>20, 50=>50, 100=>100, 200=>200, 500=>500);
	foreach ($options as $key => $value) {
		$params = array('value'=>$key);
		if($length == $key){
			$params['selected'] = 'selected';
		}
		echo html_writer::tag('option',$value, $params);
	}
	echo html_writer::end_tag('select');

	echo html_writer::tag("span", " ");

	echo html_writer::start_tag('select', array('name'=>'courseid', 'onchange'=>'this.form.submit()'));
	foreach ($courses as $key => $value) {
		$params = array('value'=>$value->id);
		if($courseid == $value->id){
			$params['selected'] = 'selected';
		}
		echo html_writer::tag('option',$value->fullname, $params);
	}
	echo html_writer::end_tag('select');

	echo html_writer::tag("span", " ");
	echo html_writer::start_tag('select', array('name'=>'quiz', 'onchange'=>'this.form.submit()'));

    $options = report_univselect_get_quiz_options($courseid);
	foreach ($options as $key => $value) {
		$params = array('value'=>$key);
		if($quiz == $key){
			$params['selected'] = 'selected';
		}
		echo html_writer::tag('option',$value, $params);
	}
	echo html_writer::end_tag('select');

	echo html_writer::empty_tag('input', array('type' => 'submit', 'value' => 'Filter'));
	echo html_writer::end_tag("label");

	echo html_writer::end_tag("form");

}

$table->out($length, true);
?>
<script>
    $(function() {
        $( ".flexible.generaltable tbody tr" ).each(function(){
        	var prevVal = 0; var i = 0;
        	$( "td", this).each(function(){
        		var curVal = $(this).html();
        		if(i > 1){
        			var html = (curVal > prevVal) ? "&#9652;":"&#9662;";
	        		$(this).html(curVal + " " + html );

	        	}
	        	prevVal = curVal;
        		i++;
        	});
        });
    });
</script>
<?php
if (!$table->is_downloading()) {
    echo $OUTPUT->footer();
}
