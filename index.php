<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 *
 * @package    report
 * @subpackage univselect
 * @copyright  2015
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require "../../config.php";
require_once($CFG->dirroot.'/report/univselect/table.php');
require_once($CFG->libdir.'/adminlib.php');

$download = optional_param('download', '', PARAM_ALPHA);
$courseid = optional_param('courseid', 0, PARAM_INT);
$userid = optional_param('userid', 0, PARAM_INT);
$groupid = optional_param('groupid', 0, PARAM_INT);
$tag0 = optional_param_array('tag0', array(), PARAM_RAW);
$tag1 = optional_param_array('tag1', array(), PARAM_RAW);
$quiz = optional_param('quiz', 0, PARAM_INT);
$practice_test = optional_param('practice_test', '', PARAM_RAW);
$length = optional_param('length', 200, PARAM_INT);

require_login();

if (is_siteadmin($USER->id)) {
	$courses = $DB->get_records_sql("SELECT * FROM mdl_course where category > 0 and visible = 1");
}else{
    $courses = $DB->get_records_sql("SELECT c.* 
                                      FROM {user_enrolments} ue, {enrol} e, {course} c 
                                      WHERE c.category > 0 AND c.visible = 1 AND c.id = e.courseid AND ue.enrolid = e.id AND ue.userid=:userid
                                      GROUP BY c.id", array('userid'=>$USER->id));
    if(empty($courses))
        print_error(get_string('not_enroled_to_course','report_univselect'));
}

if(!$courseid){
    $courseid = reset($courses)->id;
}
$student = false;
$context = context_course::instance($courseid);
if (!has_capability('report/univselect:see_all_users', $context)) {
	$userid = $USER->id;
	$student = true;
}

$sql = "SELECT ue.id, u.* FROM mdl_user u, mdl_user_enrolments ue, mdl_enrol e, mdl_role_assignments ra, mdl_context ctx WHERE ra.roleid = 5 AND e.courseid = $courseid AND u.id = ue.userid AND e.id = ue.enrolid AND ctx.instanceid = e.courseid AND ra.contextid = ctx.id AND ue.userid = ra.userid GROUP BY e.courseid, ue.userid ORDER BY u.firstname ASC";
$users = $DB->get_records_sql($sql);
$groups = groups_get_all_groups($courseid);

$PAGE->set_context($context);
$PAGE->set_url('/report/univselect/index.php', array('courseid'=>$courseid, 'userid'=>$userid, 'groupid'=>$groupid));

$table = new univselect_table1('table1', $users, $courseid, $userid, $student, $quiz, $groupid, $practice_test);
$table->is_downloading($download, get_string('univselect', 'report_univselect'), get_string('univselect1', 'report_univselect'));

/*$table_resource_list = new univselect_table5('table5', $courseid, $quiz, $length, $userid, $groupid);
$table_resource_list->is_downloading($download, get_string('univselect', 'report_univselect'), get_string('univselect1', 'report_univselect'));*/

if (!$table->is_downloading() /*&& !$table_resource_list->is_downloading()*/) {
	if (has_capability('moodle/course:update', $context)) {
		admin_externalpage_setup('reportunivselect1', '', null, '', array('pagelayout'=>'report'));
	}else{
		$PAGE->set_title(get_string('univselect1', 'report_univselect'));
		$PAGE->set_heading(get_string('univselect1', 'report_univselect'));
	}
    echo $OUTPUT->header();
    if (has_capability('moodle/course:update', $context)) {
		echo $OUTPUT->heading(get_string('univselect1', 'report_univselect'));
	}

	echo html_writer::start_tag("form",  array("action"=>$CFG->wwwroot.'/report/univselect/index.php'));
	echo html_writer::start_tag("label",  array("style"=>" margin: 20px auto;"));
	echo html_writer::tag("span", "Filter: ");
	echo html_writer::start_tag('select', array('name'=>'length'));
	$options = array(10=>10, 20=>20, 50=>50, 100=>100, 200=>200, 500=>500);
	foreach ($options as $key => $value) {
		$params = array('value'=>$key);
		if($length == $key){
			$params['selected'] = 'selected';
		}
		echo html_writer::tag('option', $value, $params);
	}
	echo html_writer::end_tag('select');
	echo html_writer::tag("span", " ");

	echo html_writer::start_tag('select', array('name'=>'courseid', 'id'=>'courseid', 'onchange'=>'document.getElementById("page").value =0;document.getElementById("userid").value =0;  document.getElementById("tag").value =""; this.form.submit()'));

	foreach ($courses as $key => $value) {
		$params = array('value'=>$value->id);
		if($courseid == $value->id){
			$params['selected'] = 'selected';
		}
		echo html_writer::tag('option',$value->fullname, $params);
	}
	echo html_writer::end_tag('select');

	echo html_writer::tag("span", " ");
	echo html_writer::start_tag('select', array('name'=>'quiz', 'onchange'=>'document.getElementById("tag").value =""; this.form.submit(); '));

    $options = report_univselect_get_quiz_options($courseid);
	foreach ($options as $key => $value) {
		$params = array('value'=>$key);
		if($quiz == $key){
			$params['selected'] = 'selected';
		}
		echo html_writer::tag('option',$value, $params);
	}
	echo html_writer::end_tag('select');

	echo html_writer::tag("span", " ");
	echo html_writer::start_tag('select', array('name'=>'practice_test', 'onchange'=>'this.form.submit(); '));

    $options = report_univselect_get_pt_options($courseid);
	foreach ($options as $key => $value) {
		$params = array('value'=>$key);
		if($practice_test == $key){
			$params['selected'] = 'selected';
		}
		echo html_writer::tag('option',$value, $params);
	}
	echo html_writer::end_tag('select');


	  echo html_writer::tag('span', '&nbsp;');
		if (has_capability('moodle/course:update', $context)) {
            echo html_writer::start_tag('select', array('name'=>'groupid', 'id'=>'groupid'));
            echo html_writer::tag('option', 'All groups', array('value'=>0));

            foreach ($groups as $key => $value) {
                $params = array('value'=>$value->id);
                if($groupid == $value->id){
                    $params['selected'] = 'selected';
                }
                echo html_writer::tag('option', format_string($value->name), $params);
            }
            echo html_writer::end_tag('select');

			  echo html_writer::start_tag('select', array('name'=>'userid', 'id'=>'userid'));
			  echo html_writer::tag('option', 'All users', array('value'=>0));

		      foreach ($users as $key => $value) {
		        $params = array('value'=>$value->id);
		        if($userid == $value->id){
		            $params['selected'] = 'selected';
		        }
		        echo html_writer::tag('option', fullname($value), $params);
		      }
		      echo html_writer::end_tag('select');
		}

        $sql_filter = report_univselect_get_sql_tag_filter($courseid,$quiz);
		$tags = $DB->get_records_sql("
                            SELECT
                              t.id,
                              t.rawname AS tagname,
                              MAX(tg.ordering) AS ordering
                            FROM
                                  mdl_quiz qz,
                                  mdl_question q
                              LEFT JOIN mdl_quiz_slots slot ON q.id = slot.questionid
                              LEFT JOIN mdl_tag_instance tg ON tg.itemtype = 'question' AND tg.itemid = q.id
                              LEFT JOIN mdl_tag t ON t.id = tg.tagid
                            
                              JOIN mdl_modules m ON m.name='quiz'
                              LEFT JOIN mdl_course_modules cm ON cm.module=m.id AND cm.instance=slot.quizid
                              LEFT JOIN mdl_tag_instance ti ON ti.component='core' AND ti.itemtype='course_modules' AND ti.itemid=cm.id
                              LEFT JOIN mdl_tag tag ON tag.id=ti.tagid
                            WHERE qz.course = $courseid AND q.id = slot.questionid AND qz.id = slot.quizid AND
                                  t.rawname != '' $sql_filter GROUP BY t.rawname ORDER BY t.rawname");

		echo html_writer::start_tag('select', array('multiple'=> true, 'name'=>'tag0[]', 'id'=>'tag'));
		echo html_writer::tag('option', 'All tags', array('value'=>0));
		foreach ($tags as $key => $value) {
		    if($value->ordering > 0){
		        continue;
            }
			$tagname = str_replace("'", "", $value->tagname);
			$params = array('value'=>$tagname);
			if(in_array($tagname, $tag0)){
			    $params['selected'] = 'selected';
			}
			echo html_writer::tag('option', $tagname, $params);
		}
		echo html_writer::end_tag('select');

		echo html_writer::start_tag('select', array('multiple'=> true, 'name'=>'tag1[]', 'id'=>'tag'));
		echo html_writer::tag('option', 'All tags', array('value'=>0));
		foreach ($tags as $key => $value) {
            if($value->ordering == 0){
                continue;
            }
			$tagname = str_replace("'", "", $value->tagname);
			$params = array('value'=>$tagname);
			if(in_array($tagname, $tag1)){
			    $params['selected'] = 'selected';
			}
			echo html_writer::tag('option', $tagname, $params);
		}
		echo html_writer::end_tag('select');


	echo html_writer::empty_tag('input', array('type' => 'submit', 'value' => 'Filter'));
	echo html_writer::end_tag("label");
	echo html_writer::end_tag("form");

	if($userid>0){
        echo univselect_grade_report_table($userid, $courseid);
    }
	echo "<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js\"></script>
    <script>
        $('select').on('touchstart',function(e) {
            e.preventDefault();
            $(this).focus();
        });
        
        $(window).ready(function() {
          $('#groupid').change(function() {
            var groupid = $(this).val();
            var courseid = $('#courseid').val();
            jQuery.ajax({
                    url: '".$CFG->wwwroot."/report/univselect/ajax.php?action=get_group_students&groupid='+groupid+'&courseid='+courseid,
                    dataType: 'json'
                }).done(function( data ) {
                    $('#userid option').detach();
                    $('#userid').append($('<option>', {value:0, text:'All users'}));
                    for (let prop in data) {
                        if(".$userid." == prop){
                            $('#userid').append($('<option>', {value: prop, text: data[prop], selected: 'selected'}));
                        }else{
                            $('#userid').append($('<option>', {value: prop, text: data[prop]}));
                        }
                    }
                });
          });
          $('#groupid').change();
        });
    </script>";
}


$table->out($length, true);
/*$table_resource_list->out($length, true);*/

if (!$table->is_downloading() /*&& !$table_resource_list->is_downloading()*/) {
    echo $OUTPUT->footer();
}
