<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 *
 * @package    report
 * @subpackage univselect
 * @copyright  2015
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['pluginname'] = 'Univselect';
$string['univselect'] = 'Univselect';
$string['univselect1'] = 'Univselect General Report IRs - MT Count';
$string['univselect2'] = 'Q Type Report';
$string['univselect3'] = 'Grade Progression';
$string['not_enroled_to_course'] = 'You are not enrolled to any course';
$string['settingsunivselect'] = 'Settings for univselect reports';
$string['reading'] = 'Reading ({$a})';
$string['writing'] = 'Writing ({$a})';
$string['math_nc'] = 'Math (NC, {$a})';
$string['math_c'] = 'Math (C, {$a})';
$string['rw_summary'] = '<b>R/W</b>';
$string['math_summary'] = '<b>Math</b>';
$string['composite'] = '<b>Composite</b>';
$string['tag_association'] = 'Tag association';
$string['grade_notify'] = 'Grade report notifications';
$string['email_to_student'] = 'Email to student';
$string['email_to_parent'] = 'Email to parent';
$string['email_to_student_desc'] = 'Email to student';
$string['email_to_parent_desc'] = 'Email to parent';
$string['email_to_student_desc_help'] = 'Email to student, in text You can use next tags:<ul><li>[[student_firstname]]</li><li>[[student_lastname]]</li><li>[[course_name]]</li></ul>';
$string['email_to_parent_desc_help'] = 'Email to parent, in text You can use next tags:<ul><li>[[student_firstname]]</li><li>[[student_lastname]]</li><li>[[course_name]]</li></ul>';
$string['email_period'] = 'Email period';
$string['email_grade_report_name'] = 'Grade Report';
$string['most-incorrect-responces'] = 'Most incorrect responces';
$string['resource_assignment_list'] = 'Resource Assignment List';
$string['course_type'] = 'Course type, like as "SAT" or "ACT"';
$string['key_english'] = 'English';
$string['key_math'] = 'Math';
$string['key_reading'] = 'Reading';
$string['key_science'] = 'Science';
$string['key_overall'] = 'Overall';
$string['course_practise_tests'] = 'Course Practise Tests';
$string['sat_tags'] = 'SAT Tags';
$string['act_tags'] = 'ACT Tags';
$string['course_practise_tests_scales'] = 'Course Practise Tests Scales';
$string['select_practise_test'] = 'Practise Test';
$string['select_quiz_type'] = 'Quiz Type';
$string['number_of_correct'] = '# Correct';
$string['score'] = 'Score';
$string['practice_test'] = 'Practice Test';
$string['quiz_type'] = 'Quiz Type';
$string['scales_saved'] = 'Scales saved';
$string['scale_reading'] = 'Reading';
$string['scale_writing'] = 'Writing';
$string['scale_math'] = 'Math';
$string['scale_english'] = 'English';
$string['scale_science'] = 'Science';

