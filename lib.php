<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 *
 * @package    report
 * @subpackage univselect
 * @copyright  2015
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();
require_once($CFG->libdir . '/pdflib.php');
require_once "table.php";


function report_univselect_cron(){
    global $DB,$CFG;

    $settings = json_decode(get_config('report_univselect', 'grade_notify_setting'));
    $duration = time() - get_config('report_univselect', 'last_cron');
    if($settings->email_period > 0 && $duration >= $settings->email_period){
        $records = $DB->get_records_sql("
                SELECT
                  CONCAT(u.id,cou.id) as unique_id, 
                  u.*,
                  cou.id as courseid,
                  cou.fullname as coursename,
                  uid.data as parents_email
                FROM {course} cou
                  LEFT JOIN {context} con ON con.contextlevel=50 AND con.instanceid=cou.id
                  LEFT JOIN {role_assignments} ra ON ra.contextid=con.id AND ra.roleid=5
                  LEFT JOIN {user} u ON u.id=ra.userid
                  LEFT JOIN {user_info_field} uif ON uif.shortname LIKE 'parentsemail'
                  LEFT JOIN {user_info_data} uid ON uif.id=uid.fieldid AND uid.userid=u.id
                WHERE cou.category > 0 AND cou.visible = 1");

        $supportuser = core_user::get_support_user();
        foreach($records as $record){
            $doc = new pdf();

            $doc->SetTitle(get_string('email_grade_report_name', 'report_univselect'));
            $doc->SetMargins(15, 15);
            $doc->AddPage();

            $c  = "<h3>Course: $record->coursename</h3>";
            $c  .= "<h3>Student: ".fullname($record)."</h3><hr><h3></h3>";
            $c  .= '<style>
                    .header {font-weight: bold;}
                    </style>';
            $c  .= univselect_grade_report_table($record->id,$record->courseid);

            $doc->writeHTML($c);
            $doc->Output($CFG->tempdir. '/grade_report.pdf','F');

            $message = str_replace('[[student_firstname]]',$record->firstname,$settings->email_to_student);
            $message = str_replace('[[student_lastname]]',$record->lastname,$message);
            $message = str_replace('[[course_name]]',$record->coursename,$message);

            email_to_user($record, $supportuser, get_string('email_grade_report_name', 'report_univselect'), $message, $message, $CFG->tempdir. '/grade_report.pdf','grade_report.pdf');

            if(!empty($record->parents_email)){
                $message = str_replace('[[student_firstname]]',$record->firstname,$settings->email_to_parent);
                $message = str_replace('[[student_lastname]]',$record->lastname,$message);
                $message = str_replace('[[course_name]]',$record->coursename,$message);

                $parents_email = explode(',',$record->parents_email);
                foreach($parents_email as $email){
                    $record->email = $email;
                    email_to_user($record, $supportuser, get_string('email_grade_report_name', 'report_univselect'), $message, $message, $CFG->tempdir. '/grade_report.pdf','grade_report.pdf');
                }
            }
        }

        set_config('last_cron', time(), 'report_univselect');
    }

    return true;
}

function report_univselect_get_quiz_options($courseid){
    $type = get_config('report_univselect','course_type_'.$courseid);
    if($type == 'act'){
        $options = array('Reading', 'English', 'Math', 'Science');
    }else{
        $options = array("Reading", "Writing", "Math (no calculator)", "Math (calculator)");
    }
    return $options;
}

function report_univselect_get_pt_options($courseid){
    $type = get_config('report_univselect','course_type_'.$courseid);
    if($type == 'act'){
        $tags = explode(',', get_config('report_univselect', 'practise_test_tags_act'));
    }else{
        $tags = explode(',', get_config('report_univselect', 'practise_test_tags_sat'));
    }
    $options = array(0=>"All Practice tests");
    sort($tags, SORT_NATURAL);
    foreach ($tags as $item) {
        $options[$item] = $item;
    };
    return $options;
}

function report_univselect_get_sql_tag_filter($courseid,$quiz){
    $type = get_config('report_univselect','course_type_'.$courseid);

    if($type == 'act'){
        if($quiz == 3){
            $sql_filter = " AND tag.rawname LIKE '%Science%'";
        }elseif($quiz == 2){
            $sql_filter = " AND tag.rawname LIKE '%Math%'";
        }elseif($quiz == 1){
            $sql_filter = " AND tag.rawname LIKE '%English%'";
        }else{
            $sql_filter = " AND tag.rawname LIKE '%Reading%'";
        }
    }else{
        if($quiz == 3){
            $sql_filter = " AND tag.rawname LIKE '%Math (calculator)%'";
        }elseif($quiz == 2){
            $sql_filter = " AND tag.rawname LIKE '%Math (no calculator)%'";
        }elseif($quiz == 1){
            $sql_filter = " AND tag.rawname LIKE '%Writing%'";
        }else{
            $sql_filter = " AND tag.rawname LIKE '%Reading%'";
        }
    }

    return $sql_filter;
}
function report_univselect_is_short_qtype($courseid,$quiz){
    $type = get_config('report_univselect','course_type_'.$courseid);

    $short = false;
    if($type == 'act'){
        if($quiz == 3){
            $short = true;
        }elseif($quiz == 2){
            $short = false;
        }elseif($quiz == 1){
            $short = true;
        }else{
            $short = true;
        }
    }else{
        if($quiz == 3){
            $short = false;
        }elseif($quiz == 2){
            $short = false;
        }elseif($quiz == 1){
            $short = true;
        }else{
            $short = true;
        }
    }

    return $short;
}
