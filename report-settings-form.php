<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 *
 * @package    report
 * @subpackage univselect
 * @copyright  2015
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir.'/formslib.php');
require_once($CFG->dirroot.'/tag/locallib.php');


class settings_univselect_course extends moodleform {
    /**
     * Form definition.
     */
    function definition() {
        global $DB;

        $mform = $this->_form;
        $courses = $DB->get_records_sql("SELECT * FROM {course} where category > 0 and visible = 1");
        $courses_arr = array();
        foreach($courses as $course){
            $courses_arr[$course->id] = $course->fullname;
        }

        $mform->addElement('header', 'moodle', get_string('tag_association', 'report_univselect'));
        $mform->addElement('select', 'courseid', get_string('course'), $courses_arr);
        $mform->setType('courseid', PARAM_INT);
        $mform->addRule('courseid', get_string('required'), 'required', null, 'client');

        $this->add_action_buttons(null, get_string('next'));
    }
}
class settings_univselect_modules extends moodleform {
    /**
     * Form definition.
     */
    function definition() {
        $mform = $this->_form;
        list($courseid) = $this->_customdata;

        $mform->addElement('hidden', 'courseid',$courseid);
        $mform->setType('courseid', PARAM_INT);
        $modinfo = get_fast_modinfo($courseid);
        $course = get_course($courseid);

        $mform->addElement('html', "<h4>Course: $course->fullname</h4>");

        $mform->addElement('header', 'general', get_string('general'));
        $mform->addElement('select', 'course_type', get_string('course_type','report_univselect'), array('sat'=>'SAT','act'=>'ACT'));
        $type = get_config('report_univselect','course_type_'.$courseid);
        if(!empty($type)){
            $mform->setDefault('course_type', $type);
        }


        foreach($modinfo->get_cms() as $cmid => $mod){
            $mform->addElement('header', 'tagsh', $mod->name);
            $mform->addElement('tags', $mod->id, get_string('tags'), array('itemtype' => 'course_modules', 'component' => 'core'));

            $tags = tag_get_tags('course_modules', $mod->id);
            $selected = array();
            foreach($tags as $tag){
                $selected[] = $tag->rawname;
            }
            $mform->setDefault($mod->id, $selected);
        }

        $this->add_action_buttons();
    }
}
class settings_univselect_grade_notify extends moodleform {
    /**
     * Form definition.
     */
    function definition() {
        $mform = $this->_form;
        $settings = json_decode(get_config('report_univselect', 'grade_notify_setting'));

        $mform->addElement('header', 'moodle', get_string('grade_notify', 'report_univselect'));

        $mform->addElement('textarea', 'email_to_student', get_string('email_to_student', 'report_univselect'));
        $mform->setType('email_to_student', PARAM_RAW);
        $mform->addHelpButton('email_to_student', 'email_to_student_desc', 'report_univselect');
        $mform->setDefault('email_to_student', (isset($settings->email_to_student)) ? $settings->email_to_student : '');

        $mform->addElement('textarea', 'email_to_parent', get_string('email_to_parent', 'report_univselect'));
        $mform->setType('email_to_parent', PARAM_RAW);
        $mform->addHelpButton('email_to_parent', 'email_to_parent_desc', 'report_univselect');
        $mform->setDefault('email_to_parent', (isset($settings->email_to_parent)) ? $settings->email_to_parent : '');

        $mform->addElement('duration', 'email_period', get_string('email_period', 'report_univselect'));
        $mform->setType('email_period', PARAM_RAW);
        $mform->setDefault('email_period', (isset($settings->email_period)) ? $settings->email_period : '');

        $this->add_action_buttons();
    }
}

class settings_univselect_course_types extends moodleform {
    /**
     * Form definition.
     */
    function definition() {
        $mform = $this->_form;

        $mform->addElement('header', 'moodle', get_string('course_practise_tests', 'report_univselect'));

        $mform->addElement('tags', 'sat', get_string('sat_tags', 'report_univselect'), array('itemtype' => 'course_modules', 'component' => 'core'));
        $selected = explode(',', get_config('report_univselect', 'practise_test_tags_sat'));
        $mform->setDefault('sat', $selected);

        $mform->addElement('tags', 'act', get_string('act_tags', 'report_univselect'), array('itemtype' => 'course_modules', 'component' => 'core'));
        $selected = explode(',', get_config('report_univselect', 'practise_test_tags_act'));
        $mform->setDefault('act', $selected);

        $this->add_action_buttons();
    }
}

class settings_univselect_select_tag_scales extends moodleform {
    /**
     * Form definition.
     */
    function definition() {
        global $DB, $CFG;
        $mform = $this->_form;

        $mform->addElement('header', 'moodle', get_string('course_practise_tests_scales', 'report_univselect'));

        $namefield = empty($CFG->keeptagnamecase) ? 'name' : 'rawname';
        $tags = $DB->get_records_menu('tag', array('tagtype' => 'official'), $namefield, 'id,' . $namefield);

        $mform->addElement('select', 'practise_test', get_string('select_practise_test','report_univselect'), $tags);

        $this->add_action_buttons(null, get_string('next'));
    }
}

class settings_univselect_insert_tag_scales extends moodleform {
    /**
     * Form definition.
     */
    function definition() {
        global $DB, $CFG;
        $mform = $this->_form;
        list($practise_test) = $this->_customdata;

        $practise_test_tag = $DB->get_record('tag', array('id' => $practise_test));
        $namefield = empty($CFG->keeptagnamecase) ? 'name' : 'rawname';
        $scales = array();
        $records = $DB->get_records('report_univselect_scales', array('practice_test_id' => $practise_test));
        foreach ($records as $record) {
            @$scales[$record->quiz_type][$record->correct] = $record->score;
        }

        $mform->addElement('hidden', 'practise_test', $practise_test);
        $mform->setType('practise_test', PARAM_INT);

        $sat_pts = explode(',', get_config('report_univselect', 'practise_test_tags_sat'));
        $mform->addElement('html', "<h4>".get_string('practice_test','report_univselect').": ".$practise_test_tag->{$namefield}."</h4>");

        if(in_array($practise_test_tag->{$namefield}, $sat_pts)) {
            $quiz_types = array('reading', 'writing', 'math');
            $mform->addElement('html', "<table><thead><tr>
                                        <th align='center'>".get_string('scale_reading','report_univselect')."</th>
                                        <th align='center'>".get_string('scale_writing','report_univselect')."</th>
                                        <th align='center'>".get_string('scale_math','report_univselect')."</th>
                                        </tr></thead><tbody><tr>");
        } else {
            $quiz_types = array('reading', 'english', 'math', 'science');
            $mform->addElement('html', "<table><thead><tr>
                                        <th align='center'>".get_string('scale_reading','report_univselect')."</th>
                                        <th align='center'>".get_string('scale_english','report_univselect')."</th>
                                        <th align='center'>".get_string('scale_math','report_univselect')."</th>
                                        <th align='center'>".get_string('scale_science','report_univselect')."</th>
                                        </tr></thead><tbody><tr>");
        }

        foreach ($quiz_types as $quiz_type) {
            $mform->addElement('html', "<td>");
            $mform->addElement('static', 'title', get_string('number_of_correct','report_univselect'), get_string('score','report_univselect'));
            for($i=1;$i<=100;$i++){
                $mform->addElement('text', 'score['.$quiz_type.']['.$i.']', $i);
                $mform->setType('score['.$quiz_type.']['.$i.']', PARAM_INT);

                if(isset($scales[$quiz_type][$i])){
                    $mform->setDefault('score['.$quiz_type.']['.$i.']', $scales[$quiz_type][$i]);
                }
            }
            $mform->addElement('html', "</td>");
        }
        $mform->addElement('html', "</tr></tbody></table>");

        $this->add_action_buttons();
    }
}
