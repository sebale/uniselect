<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 *
 * @package    report
 * @subpackage univselect
 * @copyright  2015
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require "../../config.php";
require "report-settings-form.php";

require_login();
$systemcontext   = context_system::instance();
require_capability('report/univselect:setting', $systemcontext);

$courseid = optional_param('courseid', 0, PARAM_INT);
$title = get_string('settingsunivselect', 'report_univselect');

$PAGE->set_context($systemcontext);
$PAGE->set_url('/report/univselect/report-settings.php');
$PAGE->navbar->add($title);
$PAGE->set_title($title);
$PAGE->set_heading($title);
$PAGE->set_pagelayout('admin');

$form2 = null;


if($courseid == 0){
    $form = new settings_univselect_course(null);
    $form2 = new settings_univselect_grade_notify(null);

    if (!$form2->is_cancelled() && $data = $form2->get_data()) {
        unset($data->submitbutton);
        set_config('grade_notify_setting', json_encode($data), 'report_univselect');
        set_config('last_cron', time(), 'report_univselect');
    }

    $form3 = new settings_univselect_course_types(null);
    if ($data = $form3->get_data()) {
        if (isset($data->sat)){
            set_config('practise_test_tags_sat', implode( ',', $data->sat), 'report_univselect');
        }
        if (isset($data->act)){
            set_config('practise_test_tags_act', implode( ',', (array)$data->act), 'report_univselect');
        }
    }

    $form4 = new settings_univselect_select_tag_scales(null);
    if ($data = $form4->get_data()) {
        $nexturl = new moodle_url('/report/univselect/tag-scales.php', array('practise_test' => $data->practise_test));
        redirect($nexturl);
    }
}else{
    $form = new settings_univselect_modules(null,array($courseid));

    if (!$form->is_cancelled() && $data = $form->get_data()) {
        $res = set_config('course_type_'.$courseid, $data->course_type, 'report_univselect');
        foreach($data as $mod_id=>$value){
            if(is_int($mod_id)){
                $mod_context = context_module::instance($mod_id);
                tag_set('course_modules', $mod_id, $value, 'core', $mod_context->id);
            }
        }
    }
}

echo $OUTPUT->header();
echo $OUTPUT->heading($title);

$form->display();
if($form3)
    $form3->display();
if($form4)
    $form4->display();
if($form2)
    $form2->display();

echo $OUTPUT->footer();

