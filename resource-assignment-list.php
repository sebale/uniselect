<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 *
 * @package    report
 * @subpackage univselect
 * @copyright  2015
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require "../../config.php";
require "table.php";
require_once($CFG->libdir.'/adminlib.php');

$download = optional_param('download', '', PARAM_ALPHA);
$courseid = optional_param('courseid', 0, PARAM_INT);
$quiz = optional_param('quiz', 0, PARAM_INT);
$length = optional_param('length', 100, PARAM_INT);
$tag = optional_param_array('tag', array(), PARAM_RAW);
$tsort = optional_param('tsort', '', PARAM_ALPHA);
$userid = optional_param('userid', 0, PARAM_INT);

require_login();
require_capability('report/univselect:see_all_users', context_system::instance());

if (is_siteadmin($USER->id)) {
    $courses = $DB->get_records_sql("SELECT * FROM {course} where category > 0 and visible = 1");
}else{
    $courses = $DB->get_records_sql("SELECT c.* 
                                      FROM {user_enrolments} ue, {enrol} e, {course} c 
                                      WHERE c.category > 0 AND c.visible = 1 AND c.id = e.courseid AND ue.enrolid = e.id AND ue.userid=:userid
                                      GROUP BY c.id", array('userid'=>$USER->id));
    if(empty($courses))
        print_error(get_string('not_enroled_to_course','report_univselect'));
}

if(!$courseid){
    $courseid = reset($courses)->id;
}

$context = context_course::instance($courseid);
$sql = "SELECT ue.id, u.* FROM mdl_user u, mdl_user_enrolments ue, mdl_enrol e, mdl_role_assignments ra, mdl_context ctx WHERE ra.roleid = 5 AND e.courseid = $courseid AND u.id = ue.userid AND e.id = ue.enrolid AND ctx.instanceid = e.courseid AND ra.contextid = ctx.id AND ue.userid = ra.userid GROUP BY e.courseid, ue.userid ORDER BY u.firstname ASC";
$users = $DB->get_records_sql($sql);
if (!has_capability('report/univselect:see_all_users', $context)) {
    $userid = $USER->id;
}

$PAGE->set_context($context);

$PAGE->set_url('/report/univselect/resource-assignment-list.php', array('courseid'=>$courseid, 'quiz'=>$quiz, 'userid'=>$userid));

$table = new univselect_table5('table5', $courseid, $quiz, $length, $userid);
$table->is_downloading($download, get_string('resource_assignment_list', 'report_univselect'), get_string('resource_assignment_list', 'report_univselect'));

if (!$table->is_downloading()) {
    if (has_capability('moodle/course:update', $context)) {
        admin_externalpage_setup('reportunivselect1', '', null, '', array('pagelayout'=>'report'));
    }else{
        $PAGE->set_title(get_string('resource-assignment-list', 'report_univselect'));
        $PAGE->set_heading(get_string('resource-assignment-list', 'report_univselect'));
    }
    echo $OUTPUT->header();
    echo $OUTPUT->heading(get_string('resource_assignment_list', 'report_univselect'));

    echo html_writer::start_tag("form",  array("action"=>$CFG->wwwroot.'/report/univselect/resource-assignment-list.php'));
    echo html_writer::start_tag("label",  array("style"=>" margin: 20px auto;"));
    echo html_writer::tag("span", "Filter: ");
    echo html_writer::start_tag('select', array('name'=>'length'));
    $options = array(10=>10, 20=>20, 50=>50, 100=>100, 200=>200, 500=>500);
    foreach ($options as $key => $value) {
        $params = array('value'=>$key);
        if($length == $key){
            $params['selected'] = 'selected';
        }
        echo html_writer::tag('option',$value, $params);
    }
    echo html_writer::end_tag('select');

    echo html_writer::tag("span", " ");

    echo html_writer::start_tag('select', array('name'=>'courseid', 'onchange'=>' document.getElementById("tag").value =""; this.form.submit()'));

    foreach ($courses as $key => $value) {
        $params = array('value'=>$value->id);
        if($courseid == $value->id){
            $params['selected'] = 'selected';
        }
        echo html_writer::tag('option',$value->fullname, $params);
    }
    echo html_writer::end_tag('select');

    echo html_writer::tag("span", " ");
    echo html_writer::start_tag('select', array('name'=>'quiz', 'onchange'=>' document.getElementById("tag").value =""; this.form.submit()'));
    $options = report_univselect_get_quiz_options($courseid);
    foreach ($options as $key => $value) {
        $params = array('value'=>$key);
        if($quiz == $key){
            $params['selected'] = 'selected';
        }
        echo html_writer::tag('option',$value, $params);
    }
    echo html_writer::end_tag('select');

    if (has_capability('moodle/course:update', $context)) {
        echo html_writer::start_tag('select', array('name'=>'userid', 'id'=>'userid'));
        echo html_writer::tag('option', 'All users', array('value'=>0));

        foreach ($users as $key => $value) {
            $params = array('value'=>$value->id);
            if($userid == $value->id){
                $params['selected'] = 'selected';
            }
            echo html_writer::tag('option', fullname($value), $params);
        }
        echo html_writer::end_tag('select');
    }

    $sql_filter = report_univselect_get_sql_tag_filter($courseid,$quiz);
    $tags = $DB->get_records_sql("
                            SELECT
                              t.id,
                              t.rawname AS tagname
                            FROM
                                  mdl_quiz qz,
                                  mdl_question q
                              LEFT JOIN mdl_quiz_slots slot ON q.id = slot.questionid
                              LEFT JOIN mdl_tag_instance tg ON tg.itemtype = 'question' AND tg.itemid = q.id
                              LEFT JOIN mdl_tag t ON t.id = tg.tagid
                            
                              JOIN mdl_modules m ON m.name='quiz'
                              LEFT JOIN mdl_course_modules cm ON cm.module=m.id AND cm.instance=slot.quizid
                              LEFT JOIN mdl_tag_instance ti ON ti.component='core' AND ti.itemtype='course_modules' AND ti.itemid=cm.id
                              LEFT JOIN mdl_tag tag ON tag.id=ti.tagid
                            WHERE qz.course = $courseid AND q.id = slot.questionid AND qz.id = slot.quizid AND
                                  t.rawname != '' $sql_filter GROUP BY t.rawname ORDER BY t.rawname");
    echo html_writer::start_tag('select', array('multiple'=> true, 'name'=>'tag[]', 'id'=>'tag'));
    echo html_writer::tag('option', 'All tags', array('value'=>0));
    foreach ($tags as $key => $value) {
        $tagname = str_replace("'", "", $value->tagname);
        $params = array('value'=>$tagname);
        if(in_array($tagname, $tag)){
            $params['selected'] = 'selected';
        }
        echo html_writer::tag('option', $tagname, $params);
    }
    echo html_writer::end_tag('select');

    echo html_writer::empty_tag('input', array('type' => 'submit', 'value' => 'Filter'));
    echo html_writer::end_tag("label");

    echo html_writer::end_tag("form");

    echo "<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js\"></script>
    <script>
        $('select').on('touchstart',function(e) {
            e.preventDefault();
            $(this).focus();
        });
    </script>";

}


$table->out($length, true);


if (!$table->is_downloading()) {
    echo $OUTPUT->footer();
}

