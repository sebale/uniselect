<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Settings for the univselect report
 *
 * @package    report
 * @subpackage univselect
 * @copyright  2015
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die;

if (!$ADMIN->locate('univselect')){
	$ADMIN->add('reports', new admin_category('univselect', get_string('univselect', 'report_univselect')));
	$ADMIN->add('univselect', new admin_externalpage('reportunivselect1', get_string('univselect1', 'report_univselect'), "$CFG->wwwroot/report/univselect/index.php", 'report/univselect:view'));
	$ADMIN->add('univselect', new admin_externalpage('reportunivselect2', get_string('univselect2', 'report_univselect'), "$CFG->wwwroot/report/univselect/tags.php", 'report/univselect:view'));
	$ADMIN->add('univselect', new admin_externalpage('reportunivselect3', get_string('univselect3', 'report_univselect'), "$CFG->wwwroot/report/univselect/grading.php", 'report/univselect:view'));
	$ADMIN->add('univselect', new admin_externalpage('reportunivselect4', get_string('most-incorrect-responces', 'report_univselect'), "$CFG->wwwroot/report/univselect/most-incorrect-responces.php", 'report/univselect:view'));
	//$ADMIN->add('univselect', new admin_externalpage('reportunivselect5', get_string('resource_assignment_list', 'report_univselect'), "$CFG->wwwroot/report/univselect/resource-assignment-list.php", 'report/univselect:view'));
	$ADMIN->add('univselect', new admin_externalpage('settingsunivselect', get_string('settingsunivselect', 'report_univselect'), "$CFG->wwwroot/report/univselect/report-settings.php", 'report/univselect:setting'));
}
$settings = null;
