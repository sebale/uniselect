<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 *
 * @package    report
 * @subpackage univselect
 * @copyright  2015
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir.'/tablelib.php');
require_once('lib.php');

class univselect_table1 extends table_sql {
	public $tags = array();
	public $users = array();
	public $student = false;



    function __construct($uniqueid, $users, $courseid, $userid, $student, $quizid, $groupid, $practice_test) {
		global $CFG, $DB, $USER;

        parent::__construct($uniqueid);

		$this->student = $student;
		$this->users = $users;


		$sql_select = "";
		$sql_from = "";

        $columns = array('tmp1', 'tmp2', 'tmp3', 'slot', 'answer');
        $headers = array('Practice Test', 'Subject', 'Section', 'Q#', 'Answer Key');

        $sql_filter = report_univselect_get_sql_tag_filter($courseid,$quizid);
		$this->tags = $DB->get_records_sql("SELECT
                                              q.id,
                                              GROUP_CONCAT(DISTINCT t.rawname ORDER BY tg.ordering ASC) AS tags
                                            FROM mdl_quiz_slots slot, mdl_quiz qz,
                                              mdl_question q LEFT JOIN mdl_tag_instance tg ON tg.itemtype = 'question' AND tg.itemid = q.id
                                              LEFT JOIN mdl_tag t ON t.id = tg.tagid
                                            WHERE qz.course = $courseid AND q.id = slot.questionid AND t.rawname != '' AND qz.id = slot.quizid
                                            GROUP BY q.id");

		$max = 0;
		foreach($this->tags as $item){
			$item->data = explode(",", $item->tags);
			if(count($item->data) > $max){
				$max = count($item->data);
			}
		}
		for($i = 0; $i<$max; $i++){
			$columns[] = "tag$i";
			$headers[] = "Q Type 1.$i";
			$sql_select .=", ' ' as tag$i";
		}
		foreach($this->users as $user){
			if($userid){
				if($userid != $user->id){
					unset($this->users[$user->id]);
					continue;
				}
			}elseif($groupid>0 && !groups_is_member($groupid, $user->id)){
                unset($this->users[$user->id]);
                continue;
            }
			$u = $user->id;
			$columns[] = "user$u";
			$headers[] = fullname($user);

			$sql_select .=", '' as user$u";

		}

 		$tags0 = optional_param_array('tag0', array(), PARAM_RAW);
 		$tags1 = optional_param_array('tag1', array(), PARAM_RAW);
 		//$tags = (!empty($tags1))?$tags1:$tags0;
        $tags = array_merge($tags0,$tags1);

        $stags = "";
        if($tags){
        	$sql_from .=" LEFT JOIN mdl_tag_instance tg ON tg.itemtype= 'question' AND tg.itemid = q.id";
			$sql_from .=" LEFT JOIN mdl_tag t ON t.id = tg.tagid";
        	$w = array();
        	foreach ($tags as $tag) {
        		if(!$tag){
        			$stags = "";
        			$w = array();
        			break;
        		}
	        	$w[] = "t.rawname LIKE '%$tag%'";
	        	$stags .= "&tag[]=$tag";
        	}
        	if($w){
        		$sql_filter .= "AND (".implode(' OR ', $w).")";
        	}
        }

        $sql_params = array();
        if($practice_test != '0'){
            $sql_from .=" LEFT JOIN mdl_tag_instance ti2 ON ti2.component='core' AND ti2.itemtype='course_modules' AND ti2.itemid=cm.id";
            $sql_from .=" LEFT JOIN mdl_tag tag2 ON tag2.id=ti2.tagid";
            $sql_filter .= " AND tag2.rawname LIKE '$practice_test'";
        }

		//print_r($this->users); exit;
		if(!$this->student){
			$columns[] = "ircount";
			$headers[] = "IR Count";
		}else{
			$sql_from .=" LEFT JOIN (SELECT a.questionusageid, a.quiz, a.grade
				FROM (SELECT qa.questionusageid, qt.quiz, round(sum(((qa.maxmark * qas.fraction) * qz.grade / qz.sumgrades)),0) as grade
				FROM mdl_quiz qz, mdl_question_attempts qa, mdl_quiz_attempts qt, mdl_question_attempt_steps qas WHERE qt.userid = $USER->id AND qz.id = qt.quiz AND qa.questionusageid = qt.uniqueid AND qas.questionattemptid = qa.id GROUP BY qt.id ORDER BY grade DESC) a GROUP BY a.quiz) qb ON qb.quiz = qz.id
				LEFT JOIN mdl_question_attempts qa ON qa.questionusageid = qb.questionusageid AND qa.questionid = q.id
				LEFT JOIN mdl_question_attempt_steps qas ON qas.userid = $USER->id AND qas.questionattemptid = qa.id AND qas.state != 'todo' AND qas.state != 'complete'";

			//$sql_select .= ",  as grades";
			$sql_filter .= " AND (((qa.maxmark * qas.fraction) * qz.grade / qz.sumgrades) = 0 or ((qa.maxmark * qas.fraction) * qz.grade / qz.sumgrades) IS NULL)";

		}

		$this->define_columns($columns);
		$this->define_headers($headers);

		$fields = "@s:=@s+1 as id, slot.id as slotid, slot.questionid, '0' as ircount, qz.id as qzid, q.id as qid, qz.id as tmp1, qz.id as tmp2, qz.id as tmp3, slot.slot, qz.name as quizname, qaa.answer $sql_select";
		$from = "(SELECT @s:= 0) AS s, mdl_quiz_slots slot
							 LEFT JOIN mdl_quiz qz ON qz.id = slot.quizid
							 LEFT JOIN mdl_question q ON q.id = slot.questionid
							 LEFT JOIN mdl_question_answers qaa ON qaa.question = q.id AND qaa.fraction = 1
							 JOIN mdl_modules m ON m.name='quiz'
							 LEFT JOIN mdl_course_modules cm ON cm.module=m.id AND cm.instance=qz.id
							 LEFT JOIN mdl_tag_instance ti ON ti.component='core' AND ti.itemtype='course_modules' AND ti.itemid=cm.id
							 LEFT JOIN mdl_tag tag ON tag.id=ti.tagid
							$sql_from";
		$where = "qz.course = $courseid $sql_filter ";
		$group = "GROUP BY IF(qaa.answer<=>0,qaa.answer,FORMAT(qaa.answer, 2)), qaa.question";
		//die("SELECT $fields FROM $from WHERE $where".$group);
		$this->set_sql($fields, $from, $where.$group, $sql_params);
        $this->sortable(true, 'slot', SORT_ASC);

		$this->set_count_sql("SELECT COUNT(*) FROM $from WHERE $where", $sql_params);
		$this->define_baseurl("$CFG->wwwroot/report/univselect/index.php?courseid=$courseid&userid=$userid&quiz=$quizid".$stags);
    }
    function col_answer($values)
	{
		global $DB;


		$values->answer = strip_tags($values->answer);
		foreach($this->users as $user){
			$data = $DB->get_record_sql("
				SELECT ROUND(((qa.maxmark * qas.fraction) * qz.grade / qz.sumgrades),2) as grade, qa.rightanswer as rightanswer, qa.responsesummary as responsesummary
                FROM mdl_quiz qz
                  LEFT JOIN mdl_question q ON q.id = $values->qid
                  LEFT JOIN (SELECT a.questionusageid, a.quiz, a.grade
                             FROM (SELECT qa.questionusageid, qt.quiz, round(sum(((qa.maxmark * qas.fraction) * qz.grade / qz.sumgrades)),0) as grade
                                   FROM mdl_quiz qz, mdl_question_attempts qa, mdl_quiz_attempts qt, mdl_question_attempt_steps qas WHERE qt.userid = $user->id AND qz.id = qt.quiz AND qa.questionusageid = qt.uniqueid AND qas.questionattemptid = qa.id GROUP BY qt.id ORDER BY grade DESC) a GROUP BY a.quiz) qb ON qb.quiz = qz.id
                  LEFT JOIN mdl_question_attempts qa ON qa.questionusageid = qb.questionusageid AND qa.questionid = q.id
                  LEFT JOIN mdl_question_attempt_steps qas ON qas.userid = $user->id AND qas.questionattemptid = qa.id AND qas.state != 'todo' AND qas.state != 'complete'
                WHERE qz.id = $values->qzid
				");

				$name = "user$user->id";

				if(!$this->student){
				    if(strlen($data->responsesummary) > 0){
                        $values->{$name} = ($data->grade > 0) ? 1:0;
                        $values->ircount = ($data->grade > 0) ? $values->ircount:($values->ircount+1);
                    }else{
                        $values->{$name} = '-';
                    }
				}else{
					$values->{$name} = (strlen($data->responsesummary) > 0)?$data->responsesummary:'-';
					if(!empty($data->grade) && $data->grade == 0){
					    $values->show = 1;
					}else{
                        $values->show = 0;
                    }
				}
		}

		return $values->answer;
	}
    function col_tag0($values)
	{
		if($this->tags){
			foreach($this->tags as $item){
				if($item->id == $values->questionid and !empty($item->data)){
					foreach($item->data as $key=>$d){
						$name = "tag$key";
						$values->{$name} = $d;
					}
					break;
				}
			}
		}
		return $values->tag0;
	}
    function col_tmp1($values) {
		$data = explode(",", $values->quizname);
		$val = substr(trim($data[0]),13);
       return "#".trim($val);
    }
	function col_tmp2($values) {
        return $values->quizname;
		$data = explode(",", $values->quizname);
		$val = trim($data[2]);
       return $val;
    }
	function col_tmp3($values) {
		$data = explode(",", $values->quizname);
		$val = substr(trim($data[1]),8);
       return $val;
    }

    function format_row($row) {
        if (is_array($row)) {
            $row = (object)$row;
        }
        $formattedrow = array();
        foreach (array_keys($this->columns) as $column) {
            $colmethodname = 'col_'.$column;
            if (method_exists($this, $colmethodname)) {
                $formattedcolumn = $this->$colmethodname($row);
            } else {
                $formattedcolumn = $this->other_cols($column, $row);
                if ($formattedcolumn===NULL) {
                    $formattedcolumn = $row->$column;
                }
            }
            $formattedrow[$column] = $formattedcolumn;
        }

        if(isset($formattedrow['ircount']) && $formattedrow['ircount'] == 0){
            return array();
        }elseif(isset($row->show) && $row->show == 0){
            return array();
        }
        return $formattedrow;
    }
    function add_data_keyed($rowwithkeys, $classname = '') {
        if(!empty($rowwithkeys)){
            $this->add_data($this->get_row_from_keyed($rowwithkeys), $classname);
        }
    }
}


class univselect_table2 extends table_sql {

	public $increment = 1;

	public $filter = '';
	public $course = 0;
	public $userid = 0;
	public $tags = array();
	public $quizes = array();
	public $chartdata = array();

    function __construct($uniqueid, $courseid, $quizid, $userid, $groupid) {
		global $CFG, $DB;

        parent::__construct($uniqueid);

		$sql_select = "";
		$sql_from = "";
        $this->userid = 0;
		if($userid>0){
            $this->userid = $userid;
        }elseif($groupid>0){
            $members_records = groups_get_members($groupid);
            $members = array();
            foreach($members_records as $members_record){
                $members[] = $members_record->id;
            }
            $this->userid = implode(',', $members);
        }

        $columns = array();
        $headers = array();

        $sql_filter = report_univselect_get_sql_tag_filter($courseid,$quizid);

        $this->filter = $sql_filter;
        $this->course = $courseid;
        $tags = optional_param_array('tag', array(), PARAM_RAW);
        $sql_where = "";
        $stags = "";
        if($tags){
        	$w = array();
        	foreach ($tags as $tag) {
        		if(!$tag){
        			$stags = "";
        			$w = array();
        			break;
        		}
	        	$w[] = "b.tags LIKE '%$tag%'";
	        	$stags .= "&tag[]=$tag";
        	}
        	if($w){
        		$sql_where = "AND (".implode(' OR ', $w).")";
        	}
        }

		$this->quizes = $DB->get_records_sql("
                                SELECT
                                  qz.id,
                                  SUBSTRING_INDEX(qz.name, ',', 1) AS name
                                FROM mdl_quiz qz,  mdl_modules m, mdl_course_modules cm
                                  LEFT JOIN mdl_tag_instance ti ON ti.component='core' AND ti.itemtype='course_modules' AND ti.itemid=cm.id
                                  LEFT JOIN mdl_tag tag ON tag.id=ti.tagid
                                WHERE m.name = 'quiz' AND cm.module = m.id AND cm.visible = 1 AND qz.id = cm.instance AND
                                      qz.course = $courseid $sql_filter ORDER BY CAST(SUBSTRING_INDEX(SUBSTRING_INDEX(qz.name, ',', 1), ' ', -1) AS unsigned)");

		$this->tags = $DB->get_records_sql("
                            SELECT
                              q.id,
                              GROUP_CONCAT(DISTINCT t.rawname ORDER BY tg.ordering ASC) AS tags
                            FROM
                              mdl_quiz qz,
                              mdl_question q
                              LEFT JOIN mdl_quiz_slots slot ON q.id = slot.questionid
                            
                              LEFT JOIN mdl_tag_instance tg ON tg.itemtype = 'question' AND tg.itemid = q.id
                              LEFT JOIN mdl_tag t ON t.id = tg.tagid
                            
                              JOIN mdl_modules m ON m.name = 'quiz'
                              LEFT JOIN mdl_course_modules cm ON cm.module = m.id AND cm.instance = slot.quizid
                              LEFT JOIN mdl_tag_instance ti ON ti.component = 'core' AND ti.itemtype = 'course_modules' AND ti.itemid = cm.id
                              LEFT JOIN mdl_tag tag ON tag.id = ti.tagid
                            WHERE qz.course = $courseid AND q.id = slot.questionid AND t.rawname != '' $sql_filter
                            GROUP BY q.id");

		$max = 0;
		foreach($this->tags as $item){
			$item->data = explode(",", $item->tags);
			if(count($item->data) > $max){
				$max = count($item->data);
			}
		}
		if(report_univselect_is_short_qtype($courseid,$quizid)){
            $sql_qtype = 'AND tg.ordering=0';
            $max = min($max,1);
        }else{
            $sql_qtype = 'AND tg.ordering<2';
            $max = min($max,2);
        }

		for($i = 0; $i<$max; $i++){
			$columns[] = "tag$i";
			$headers[] = "Q Type 1.$i";
			if($i>0){
                $sql_select .= ", ' ' as tag$i";
            }
		}


		$this->chartdata[0][] = "Quiz name";
		$this->chartdata[0]['id'] = 0;
		$i = 1;
		foreach($this->quizes as $quiz){
			$u = $quiz->id;
			$columns[] = "quiz$u";
			$headers[] = $quiz->name;
			$this->chartdata[$i][] = $quiz->name;
			$this->chartdata[$i]['id'] = $quiz->id;
			$sql_select .=", u$u.id as quiz$u";
			$sql_from .= " LEFT JOIN mdl_quiz u$u ON u$u.id = $u";
			$i++;
		}


		$this->define_columns($columns);
		$this->define_headers($headers);

		$fields = "b.id, b.questions, b.tags, b.tags as tag0 $sql_select";
		$from = "(SELECT
                    a.id,
                    GROUP_CONCAT(DISTINCT a.id) AS questions,
                    a.tags
                  FROM (SELECT
                          q.id,
                          GROUP_CONCAT(DISTINCT t.rawname ORDER BY tg.ordering ASC) AS tags
                        FROM mdl_quiz_slots slot
                          LEFT JOIN mdl_quiz qz ON qz.id = slot.quizid
                          LEFT JOIN mdl_question q ON q.id = slot.questionid
                          LEFT JOIN mdl_tag_instance tg ON tg.itemtype = 'question' AND tg.itemid = q.id $sql_qtype
                          LEFT JOIN mdl_tag t ON t.id = tg.tagid
            
                          JOIN mdl_modules m ON m.name = 'quiz'
                          LEFT JOIN mdl_course_modules cm ON cm.module = m.id AND cm.instance = slot.quizid
                          LEFT JOIN mdl_tag_instance ti ON ti.component = 'core' AND ti.itemtype = 'course_modules' AND ti.itemid = cm.id
                          LEFT JOIN mdl_tag tag ON tag.id = ti.tagid
                        WHERE qz.course = $courseid AND q.id = slot.questionid AND qz.id = slot.quizid AND t.rawname != ''
                              $sql_filter
                        GROUP BY q.id) a
                  GROUP BY tags) b $sql_from";

		$where = " b.id > 0 $sql_where";
		//die("SELECT $fields FROM $from WHERE  $where");
		$this->set_sql($fields, $from, $where, array());
		$this->define_baseurl("$CFG->wwwroot/report/univselect/tags.php?courseid=$courseid&quiz=$quizid&userid=$userid".$stags);
    }

    function col_tag0($values)
    {
    	global $DB, $USER;

    	$tags = explode(",", $values->tags);

    	foreach ($tags as $key => $val) {
    		$values->{"tag".$key} = $val;
    	}
    	if($this->quizes and $values->questions){
    		$this->chartdata[0][$values->tags] = $values->tags;


    		foreach($this->quizes as $quiz){
    			$data = $DB->get_record_sql("SELECT COUNT(DISTINCT IF(((qa.maxmark * qas.fraction) * qz.grade / qz.sumgrades) > 0, null, q.id)) AS answers FROM
					mdl_quiz_slots slot
					LEFT JOIN mdl_quiz qz ON qz.id = slot.quizid
					LEFT JOIN mdl_question q ON q.id = slot.questionid
					LEFT JOIN mdl_question_answers qaa ON qaa.question = q.id AND qaa.fraction = 1

					LEFT JOIN (SELECT a.questionusageid, a.quiz, a.grade
									FROM (SELECT qa.questionusageid, qt.quiz, round(sum(((qa.maxmark * qas.fraction) * qz.grade / qz.sumgrades)),0) as grade
									FROM mdl_quiz qz, mdl_question_attempts qa, mdl_quiz_attempts qt, mdl_question_attempt_steps qas WHERE qt.userid IN({$this->userid}) AND qz.id = qt.quiz AND qa.questionusageid = qt.uniqueid AND qas.questionattemptid = qa.id GROUP BY qt.id ORDER BY grade DESC) a GROUP BY a.quiz) qb ON qb.quiz = qz.id
					LEFT JOIN mdl_question_attempts qa ON qa.questionusageid = qb.questionusageid AND qa.questionid = q.id
					LEFT JOIN mdl_question_attempt_steps qas ON qas.userid IN({$this->userid}) AND qas.questionattemptid = qa.id AND qas.state != 'todo' AND qas.state != 'complete'
					
					JOIN mdl_modules m ON m.name = 'quiz'
                    LEFT JOIN mdl_course_modules cm ON cm.module = m.id AND cm.instance = slot.quizid
                    LEFT JOIN mdl_tag_instance ti ON ti.component = 'core' AND ti.itemtype = 'course_modules' AND ti.itemid = cm.id
                    LEFT JOIN mdl_tag tag ON tag.id = ti.tagid

					WHERE q.id IN ($values->questions) AND qas.userid IS NOT NULL AND qz.id = {$quiz->id} AND qz.course = {$this->course} AND q.id = slot.questionid AND qz.id = slot.quizid $this->filter");

    			$values->{"quiz".$quiz->id} = (int)$data->answers;
    			//$this->chartdata[$this->increment][] = $values->tags;

    			if(!isset($this->chartdata[$this->increment][$quiz->name])){
					//$this->chartdata[$this->increment][] = $quiz->name;
    			}
    			foreach($this->chartdata as &$r){
    				if($r['id'] == $quiz->id){
    					$r[$values->tags] = (int)$data->answers;
    				}
    			}

    			//$this->chartdata[$this->increment][] = (int)$data->answers;
    		}

    		$this->increment = $this->increment + 1;
    	}
    	//$this->chartdata['header'][] = $values->tags;

    	//print_r($values);exit;
       	return $values->tag0;
    }
}



class univselect_table3 extends table_sql {

	public $increment = 0;
	public $course = 0;
	public $tags = array();
	public $quizes = array();
	public $chartdata = array();

    function __construct($uniqueid, $courseid, $quizid) {
		global $CFG, $DB;

        parent::__construct($uniqueid);

        $sql_filter = report_univselect_get_sql_tag_filter($courseid,$quizid);

		$sql_select = "";
		$sql_from = "";

        $columns = array('firstname', 'lastname');
        $headers = array('First name', 'Last name');

        $this->course = $courseid;

		$this->quizes = $DB->get_records_sql("
                                SELECT
                                  qz.id,
                                  qz.name
                                FROM mdl_quiz qz, mdl_modules m, mdl_course_modules cm
                                  LEFT JOIN mdl_tag_instance ti ON ti.component = 'core' AND ti.itemtype = 'course_modules' AND ti.itemid = cm.id
                                  LEFT JOIN mdl_tag tag ON tag.id = ti.tagid
                                WHERE m.name = 'quiz' AND cm.module = m.id AND cm.visible = 1 AND qz.id = cm.instance AND qz.course = $courseid $sql_filter");

		foreach($this->quizes as $quiz){
			$q = $quiz->id;
			$columns[] = "quiz$q";
			$name = explode(",", $quiz->name);
			$test = explode(" ", $name[0]);

			@$type = (substr(trim($name[2]),0,1) == "M")?"Mnc":substr(trim($name[2]),0,1);
			$headers[] = substr(trim($test[0]),0,1) . substr(trim($test[1]),0,1) . substr(trim($test[2]),0,1) . ", " . $type;

			$sql_select .=", ROUND(((g$q.finalgrade/g$q.rawgrademax)*100),2) as quiz$q";
			//$sql_select .=", ROUND(((h$q.finalgrade/h$q.rawgrademax)*100),2) as hquiz$q";

			$sql_from .= " LEFT JOIN mdl_quiz u$q ON u$q.id = $q";
			$sql_from .= " LEFT JOIN mdl_grade_items gi$q ON gi$q.iteminstance = $q AND gi$q.itemmodule = 'quiz'";
			$sql_from .= " LEFT JOIN mdl_grade_grades g$q ON g$q.itemid = gi$q.id AND g$q.userid = u.id";
			//$sql_from .= " LEFT JOIN (SELECT h.* FROM (SELECT g.id, g.userid, g.itemid, g.rawgrade, g.finalgrade FROM `mdl_grade_grades_history` g WHERE g.source LIKE 'mod/quiz' ORDER BY g.userid, g.itemid, g.timemodified DESC) h GROUP BY h.userid, h.itemid) h$q ON h$q.itemid = gi$q.id AND h$q.userid = u.id";
		}
		//print_r($this->users); exit;
		$this->define_columns($columns);
		$this->define_headers($headers);

		$fields = "u.id, u.firstname, u.lastname $sql_select";
		$from = "(SELECT distinct ue.userid FROM mdl_user_enrolments ue, mdl_enrol e WHERE e.id = ue.enrolid AND e.courseid = $courseid) ue, mdl_user u $sql_from";

		$where = "u.id = ue.userid";
		//die("SELECT $fields FROM $from $where");
		$this->set_sql($fields, $from, $where, array());
		$this->define_baseurl("$CFG->wwwroot/report/univselect/grading.php?courseid=$courseid&quiz=$quizid");
    }

    function col_firstname($values)
    {
    	global $DB, $USER;

    	if($this->quizes){
    		foreach($this->quizes as $quiz){
    			$name = "quiz".$quiz->id;
    			$values->{$name} = (float)$values->{$name};
    		}
    	}
       	return $values->firstname;
    }
}

class univselect_table4 extends table_sql {

    public $increment = 1;

    public $filter = '';
    public $course = 0;
    public $tags = array();
    public $quizes = array();

    function __construct($uniqueid, $courseid, $quizid, $set_order) {
        global $CFG, $DB;

        parent::__construct($uniqueid);

        $sql_select = "";

        $columns = array('slot','practice_test');
        $headers = array('Q#','Practice Test');

        $sql_filter = report_univselect_get_sql_tag_filter($courseid,$quizid);

        $this->filter = $sql_filter;
        $this->course = $courseid;
        $tags = optional_param_array('tag', array(), PARAM_RAW);
        $sql_where = "";
        $stags = "";
        if($tags){
            $w = array();
            foreach ($tags as $tag) {
                if(!$tag){
                    $stags = "";
                    $w = array();
                    break;
                }
                $w[] = "q_tags.tags LIKE '%$tag%'";
                $stags .= "&tag[]=$tag";
            }
            if($w){
                $sql_where = "AND (".implode(' OR ', $w).")";
            }
        }


        $this->tags = $DB->get_records_sql("
                            SELECT
                              q.id,
                              GROUP_CONCAT(DISTINCT t.rawname ORDER BY tg.ordering ASC) AS tags
                            FROM
                              mdl_quiz qz,
                              mdl_question q
                              LEFT JOIN mdl_quiz_slots slot ON q.id = slot.questionid
                            
                              LEFT JOIN mdl_tag_instance tg ON tg.itemtype = 'question' AND tg.itemid = q.id
                              LEFT JOIN mdl_tag t ON t.id = tg.tagid
                            
                              JOIN mdl_modules m ON m.name = 'quiz'
                              LEFT JOIN mdl_course_modules cm ON cm.module = m.id AND cm.instance = slot.quizid
                              LEFT JOIN mdl_tag_instance ti ON ti.component = 'core' AND ti.itemtype = 'course_modules' AND ti.itemid = cm.id
                              LEFT JOIN mdl_tag tag ON tag.id = ti.tagid
                            WHERE qz.course = $courseid AND q.id = slot.questionid AND t.rawname != '' $sql_filter
                            GROUP BY q.id");

        $max = 0;
        foreach($this->tags as $item){
            $item->data = explode(",", $item->tags);
            if(count($item->data) > $max){
                $max = count($item->data);
            }
        }
        for($i = 0; $i<$max; $i++){
            $columns[] = "tag$i";
            $headers[] = "Q Type 1.$i";
            if($i>0){
                $sql_select .= ", ' ' as tag$i";
            }
        }

        $columns[] = 'rightanswer';
        $headers[] = 'Right answer';
        $columns[] = 'avg_fraction';
        $headers[] = 'Average grade';

        $this->define_columns($columns);
        $this->define_headers($headers);

        $fields = "que.id,
                  que.name,
                  que.defaultmark AS fraction,
                  qa.rightanswer,
                  qs.slot,
                  q_tags.tags,
                  q_tags.tags as tag0,
                  AVG(qas.fraction) AS avg_fraction,
                  tagp.rawname AS practice_test $sql_select";
        $from = "mdl_quiz q
              LEFT JOIN mdl_quiz_slots qs ON qs.quizid = q.id
              LEFT JOIN mdl_question que ON que.id = qs.questionid
            
              JOIN mdl_modules m ON m.name = 'quiz'
              LEFT JOIN mdl_course_modules cm ON cm.module = m.id AND cm.instance = q.id
              LEFT JOIN mdl_tag_instance ti ON ti.component = 'core' AND ti.itemtype = 'course_modules' AND ti.itemid = cm.id
              LEFT JOIN mdl_tag tag ON tag.id = ti.tagid              

              JOIN mdl_tag_instance tip ON tip.component = 'core' AND tip.itemtype = 'course_modules' AND tip.itemid = cm.id
              LEFT JOIN mdl_tag tagp ON tagp.id = tip.tagid AND tagp.rawname LIKE '%Practice Test%'
            
              LEFT JOIN mdl_context ctx ON ctx.contextlevel=70 AND ctx.instanceid=cm.id
              LEFT JOIN mdl_question_usages qu ON qu.contextid=ctx.id AND qu.component='mod_quiz'
              LEFT JOIN mdl_question_attempts qa ON qa.questionid = qs.questionid AND qa.questionusageid=qu.id
              LEFT JOIN mdl_question_attempt_steps qas ON qas.questionattemptid = qa.id AND qas.sequencenumber = 2
            
              LEFT JOIN (SELECT
                           tg.itemid                        AS questionid,
                           GROUP_CONCAT(DISTINCT t.rawname) AS tags
                         FROM mdl_tag_instance tg LEFT JOIN mdl_tag t ON t.id = tg.tagid
                         WHERE tg.itemtype = 'question'
                         GROUP BY tg.itemid) AS q_tags ON q_tags.questionid = qs.questionid
              ";

        $sql_order = '';
        if($set_order)
            $sql_order = ' ORDER BY avg_fraction ASC';

        $where = " q.course=:course AND tagp.id IS NOT NULL AND qa.rightanswer IS NOT NULL $sql_where $sql_filter
		            GROUP BY que.id,tagp.rawname $sql_order";
        //die("SELECT $fields FROM $from WHERE $where");

        $this->countsql = "SELECT COUNT(1) FROM(SELECT $fields FROM $from WHERE $where) AS a";
        $this->countparams = array('course'=>$courseid);

        $this->set_sql($fields, $from, $where, array('course'=>$courseid));
        $this->define_baseurl("$CFG->wwwroot/report/univselect/most-incorrect-responces.php?courseid=$courseid&quiz=$quizid".$stags);
    }

    function col_tag0($values)
    {
        global $DB, $USER;

        $tags = explode(",", $values->tags);

        foreach ($tags as $key => $val) {
            $values->{"tag".$key} = $val;
        }

        return $values->tag0;
    }

    function col_slot($values)
    {
        return 'Q.'.$values->slot.' /'.round($values->fraction,2);
    }
    function col_avg_fraction($values)
    {
        return round($values->avg_fraction,2);
    }
}

class univselect_table5 extends table_sql {

    public $increment = 1;

    public $filter = '';
    public $course = 0;
    public $tags = array();
    public $quizes = array();

    function __construct($uniqueid, $courseid, $quizid, $length, $userid, $groupid=0) {
        global $CFG, $DB, $SESSION;

        parent::__construct($uniqueid);

        $sql_filter = report_univselect_get_sql_tag_filter($courseid,$quizid);

        $tags0 = optional_param_array('tag0', array(), PARAM_RAW);
        $tags1 = optional_param_array('tag1', array(), PARAM_RAW);
        //$tags = (!empty($tags1))?$tags1:$tags0;
        $tags = array_merge($tags0,$tags1);
        $stags = "";
        if($tags){
            $w = array();
            foreach ($tags as $tag) {
                if(!$tag){
                    $stags = "";
                    $w = array();
                    break;
                }
                $w[] = "q_tags.tags LIKE '%$tag%'";
                $stags .= "&tag[]=$tag";
            }
            if($w){
                $sql_filter .= "AND (".implode(' OR ', $w).")";
            }
        }

        $tags = $DB->get_records_sql("
                            SELECT
                              q.id,
                              GROUP_CONCAT(DISTINCT t.rawname ORDER BY tg.ordering ASC) AS tags
                            FROM
                              mdl_quiz qz,
                              mdl_question q
                              LEFT JOIN mdl_quiz_slots slot ON q.id = slot.questionid
                            
                              LEFT JOIN mdl_tag_instance tg ON tg.itemtype = 'question' AND tg.itemid = q.id
                              LEFT JOIN mdl_tag t ON t.id = tg.tagid
                            
                              JOIN mdl_modules m ON m.name = 'quiz'
                              LEFT JOIN mdl_course_modules cm ON cm.module = m.id AND cm.instance = slot.quizid
                              LEFT JOIN mdl_tag_instance ti ON ti.component = 'core' AND ti.itemtype = 'course_modules' AND ti.itemid = cm.id
                              LEFT JOIN mdl_tag tag ON tag.id = ti.tagid
                              
                              LEFT JOIN (SELECT tg.itemid AS questionid, GROUP_CONCAT(DISTINCT t.rawname) AS tags
                                   FROM mdl_tag_instance tg LEFT JOIN mdl_tag t ON t.id = tg.tagid
                                   WHERE tg.itemtype = 'question'
                                   GROUP BY tg.itemid) AS q_tags ON q_tags.questionid=q.id
                            WHERE qz.course = $courseid AND q.id = slot.questionid AND t.rawname != '' $sql_filter
                            GROUP BY q.id");

        $max = 0;
        foreach($tags as $item){
            $item->data = explode(",", $item->tags);
            if(count($item->data) > $max){
                $max = count($item->data);
            }
        }
        if(report_univselect_is_short_qtype($courseid,$quizid)){
            $sql_qtype = 'AND tg.ordering=0';
            $max = min($max,1);
        }else{
            $sql_qtype = 'AND tg.ordering<2';
            $max = min($max,2);
        }

        $columns = array('practice_test');
        $headers = array('Practice test');
        $sql_select = '';
        for($i = 0; $i<$max; $i++){
            if($i>0){
                $sql_select .=", ' ' as tag$i";
            }
            $headers[] = "Q Type 1.$i";
            $columns[] = "tag$i";
        }

        $columns = array_merge($columns,array('right_answers','questions','sum'));
        $headers = array_merge($headers, array('IR Frequency #', 'Stat Frequency #','Product #'));

        $this->define_columns($columns);
        $this->define_headers($headers);

        $fields = "CONCAT_WS('',t.id,tag2.id) as uniqueid,
                   tag2.rawname as practice_test,
                   q_tags.tags as tag0,
                   t.id,
                   t.rawname,
                   ((COUNT(DISTINCT qas.id)/100)+1) AS right_answers,
                   ((q_tags_all.questions/100)+1) AS questions,
                   ((COUNT(DISTINCT qas.id)/100)+1) * ((q_tags_all.questions/100)+1) as sum
                   $sql_select";


        $user = '';
        $params = array();
        if($userid>0){
            $user = 'AND qas.userid='.$userid;
        }elseif($groupid>0){
            $members_records = groups_get_members($groupid);
            $members = array();
            foreach($members_records as $members_record){
                $members[] = $members_record->id;
            }
            if(!empty($members)){
                list($sql, $params) = $DB->get_in_or_equal($members, SQL_PARAMS_NAMED, 'user');
                $user = 'AND qas.userid ' . $sql;
            }
        }

        $from = "mdl_quiz qz
                  LEFT JOIN mdl_quiz_slots slot ON slot.quizid = qz.id
                  LEFT JOIN mdl_question q ON q.id = slot.questionid
                
                  LEFT JOIN mdl_tag_instance tg ON tg.itemtype = 'question' AND tg.itemid = q.id $sql_qtype
                  LEFT JOIN mdl_tag t ON t.id = tg.tagid
                
                  JOIN mdl_modules m ON m.name = 'quiz'
                  LEFT JOIN mdl_course_modules cm ON cm.module = m.id AND cm.instance = slot.quizid
                  LEFT JOIN mdl_tag_instance ti ON ti.component = 'core' AND ti.itemtype = 'course_modules' AND ti.itemid = cm.id
                  LEFT JOIN mdl_tag tag ON tag.id = ti.tagid
                  
                  LEFT JOIN mdl_tag_instance ti2 ON ti2.component='core' AND ti2.itemtype='course_modules' AND ti2.itemid=cm.id
                  LEFT JOIN mdl_tag tag2 ON tag2.id=ti2.tagid AND tag2.rawname LIKE '%Practice Test%'
                
                  LEFT JOIN mdl_context ctx ON ctx.contextlevel=70 AND ctx.instanceid=cm.id
                  LEFT JOIN mdl_question_usages qu ON qu.contextid=ctx.id AND qu.component='mod_quiz'
                  LEFT JOIN mdl_question_attempts qa ON qa.questionid = slot.questionid AND qa.questionusageid=qu.id
                  LEFT JOIN mdl_question_attempt_steps qas ON qas.questionattemptid = qa.id AND qas.state = 'gradedright' $user                
                
                  LEFT JOIN (SELECT
                               tg.itemid AS questionid,
                               GROUP_CONCAT(DISTINCT t.rawname) AS tags
                             FROM mdl_tag_instance tg 
                                LEFT JOIN mdl_tag t ON t.id = tg.tagid
                             WHERE tg.itemtype = 'question' $sql_qtype
                             GROUP BY tg.itemid) AS q_tags ON q_tags.questionid = q.id              
                
                  LEFT JOIN (SELECT all_tags.tags, COUNT(DISTINCT all_tags.questionid) as questions
                                FROM(
                                    SELECT 
                                        tg.itemid AS questionid, 
                                        GROUP_CONCAT(DISTINCT t.rawname) AS tags
                                     FROM mdl_tag_instance tg 
                                        LEFT JOIN mdl_tag t ON t.id = tg.tagid
                                        
                                        JOIN mdl_quiz_slots slot ON slot.questionid = tg.itemid
                                        JOIN mdl_modules m ON m.name = 'quiz'
                                        JOIN mdl_course_modules cm ON cm.module = m.id AND cm.instance = slot.quizid
                  
                                        JOIN mdl_tag_instance ti2 ON ti2.component='core' AND ti2.itemtype='course_modules' AND ti2.itemid=cm.id
                                        JOIN mdl_tag tag2 ON tag2.id=ti2.tagid AND tag2.rawname LIKE '%Practice Test%'
                                        
                                        JOIN mdl_config_plugins cp ON cp.plugin='report_univselect' AND cp.name LIKE CONCAT('course_type_', cm.course) AND cp.value='sat'
                                        
                                     WHERE tg.itemtype = 'question' $sql_qtype
                                     GROUP BY tg.itemid
                                     ) AS all_tags
                                GROUP BY all_tags.tags
                                ) AS q_tags_all ON q_tags_all.tags = q_tags.tags
                  ";

        $where = " qz.course = $courseid AND q.id = slot.questionid AND t.rawname != '' AND tag2.rawname IS NOT NULL $sql_filter 
                    GROUP BY q_tags.tags,tag2.rawname";

        $order = '';
        if(!isset($SESSION->flextable[$uniqueid]->sortby) || empty($SESSION->flextable[$uniqueid]->sortby)){
            $order = ' ORDER BY practice_test ASC, sum DESC';
        }

        //die("SELECT $fields FROM $from WHERE  $where");
        $this->countsql = "SELECT COUNT(1) FROM(SELECT $fields FROM $from WHERE $where $order) AS a";
        $this->countparams = $params;
        $this->set_sql($fields, $from, $where.$order, $params);
        $this->define_baseurl("$CFG->wwwroot/report/univselect/resource-assignment-list.php?courseid=$courseid&quiz=$quizid&length=$length&userid=$userid".$stags);
    }
    function col_tag0($values)
    {
        global $DB, $USER;

        $tags = explode(",", $values->tag0);

        foreach ($tags as $key => $val) {
            $values->{"tag".$key} = $val;
        }

        return $values->tag0;
    }

    function col_right_answers($values)
    {
        return round($values->right_answers,2);
    }

    function col_questions($values)
    {
        return round($values->questions,2);
    }

    function col_sum($values)
    {
        return round($values->sum,3);
    }
}

function univselect_grade_report_table($userid,$courseid){
    $type = get_config('report_univselect','course_type_'.$courseid);
    if($type == 'act'){
        $data = univselect_grade_report_table_act($userid,$courseid);
    }else{
        $data = univselect_grade_report_table_sat($userid,$courseid);
    }
    return $data;
}

function univselect_grade_report_table_sat($userid,$courseid){
    global $DB;

    $pt_tags = explode(',', get_config('report_univselect', 'practise_test_tags_sat'));
    list($insql, $inparams) = $DB->get_in_or_equal($pt_tags, SQL_PARAMS_NAMED);

    $records = $DB->get_records_sql("
        SELECT
          ti.id,
          GROUP_CONCAT(t.rawname) as tags,
          q.name,
          q.sumgrades as max_grade,
          IF(q.grademethod = 1,MAX(qa.sumgrades),AVG(qa.sumgrades)) as sumgrades
        
        FROM mdl_tag t
          LEFT JOIN mdl_tag_instance ti ON ti.component='core' AND ti.itemtype='course_modules' AND ti.tagid=t.id
          LEFT JOIN mdl_course_modules cm ON cm.id=ti.itemid
          LEFT JOIN mdl_quiz q ON q.id=cm.instance
          LEFT JOIN mdl_quiz_attempts qa ON qa.quiz=cm.instance AND qa.userid=:userid
        WHERE cm.id IS NOT NULL AND cm.course=:courseid AND (t.rawname $insql OR t.rawname LIKE '%Math%' OR t.rawname LIKE '%Reading%' OR t.rawname LIKE '%Writing%')
        GROUP BY cm.id, qa.quiz, qa.userid
        ", array_merge(array('courseid'=>$courseid,'userid'=>$userid),$inparams));

    $user_fields = $DB->get_records_sql("
        SELECT
          uif.shortname,
          uid.data
        FROM mdl_user_info_field uif
          LEFT JOIN mdl_user_info_data uid ON uid.fieldid=uif.id
        WHERE uif.shortname IN ('readingscore','writingscore','mathcscore','mathncscore','rwsummary','mathscore') AND uid.userid=:userid", array('userid'=>$userid));

    $data = array('reading'=>array(),'writing'=>array(),'rw_summary'=>array(),'math_nc'=>array(),'math_c'=>array(),'math_summary'=>array(),'composite'=>array());
    //$scales = univselect_get_scaled_scores();
    $scales = array();
    //$head = array('SCORE TRACKER');
    $head = array();
    foreach($records as $record){
        $tags = explode(',', $record->tags);

        $first_key = '';
        $second_key = '';
        $scale_key = '';
        foreach($tags as $tag){
            switch($tag){
                case 'Reading':
                    $first_key = 'reading';
                    $scale_key = 'reading';
                    break;
                case 'Writing':
                    $first_key = 'writing';
                    $scale_key = 'writing';
                    break;
                case 'Math (no calculator)':
                    $first_key = 'math_nc';
                    $scale_key = 'math';
                    break;
                case 'Math (calculator)':
                    $first_key = 'math_c';
                    $scale_key = 'math';
                    break;
                default:
                    $second_key = $tag;
                    break;
            }
        }

        if(!isset($scales[$second_key])){
            $scales[$second_key] = univselect_get_scaled_scores($second_key);
        }

        if(empty($data[$first_key][$second_key]))
            $data[$first_key][$second_key] = array('count'=>0,'grademax'=>0,'finalgrade'=>0);

        $record->max_grade = (int)$record->max_grade;
        $record->sumgrades = (int)$record->sumgrades;
        $data[$first_key][$second_key]['count']++;
        @$data[$first_key][$second_key]['grademax'] += $scales[$second_key][$scale_key][$record->max_grade];

        if($first_key == 'reading' || $first_key == 'writing'){
            @$data['rw_summary'][$second_key]['max'] += $scales[$second_key][$scale_key][$record->max_grade];
            @$data['rw_summary'][$second_key]['sumgrades'] += $scales[$second_key][$scale_key][$record->sumgrades];
            @$data['composite'][$second_key]['max'] += $scales[$second_key][$scale_key][$record->max_grade];
            @$data['composite'][$second_key]['sumgrades'] += $scales[$second_key][$scale_key][$record->sumgrades];
            $data[$first_key][$second_key]['finalgrade'] += $scales[$second_key][$scale_key][$record->sumgrades];
        }elseif($first_key == 'math_nc' || $first_key == 'math_c'){
            @$data['math_summary'][$second_key]['sumgrades'] += $record->sumgrades;
            @$data['math_summary'][$second_key]['max'] += $record->max_grade;
            $data[$first_key][$second_key]['finalgrade'] = round(($record->sumgrades*100)/$record->max_grade,2);
        }

        $head[preg_replace('/[^0-9]/i', '', $second_key)] = $second_key;
    }
    ksort($head);

    foreach($data['math_summary'] as $key=>$value){
        @$data['composite'][$key]['max'] += $scales[$key]['math'][$data['math_summary'][$key]['max']];
        @$data['math_summary'][$key]['sumgrades'] = $scales[$key]['math'][$data['math_summary'][$key]['sumgrades']];
        @$data['math_summary'][$key]['max'] = $scales[$key]['math'][$data['math_summary'][$key]['max']];
        @$data['composite'][$key]['sumgrades'] += $data['math_summary'][$key]['sumgrades'];
    }

    $table = new html_table();
    $table->head = array_merge(array('SCORE TRACKER','Original Score'),$head);

    foreach($data as $key=>$value){
        switch($key){
            case 'math_nc':
                @$row = array($user_fields['mathncscore']->data);
                break;
            case 'math_c':
                @$row = array($user_fields['mathcscore']->data);
                break;
            case 'writing':
                @$row = array($user_fields['writingscore']->data);
                break;
            case 'reading':
                @$row = array($user_fields['readingscore']->data);
                break;
            case 'rw_summary':
                @$row = array($user_fields['rwsummary']->data);
                break;
            case 'math_summary':
                @$row = array($user_fields['mathscore']->data);
                break;
            default:
                $row = array('-');
                break;
        }

        $count = 0;
        if($key == 'math_nc' || $key == 'math_c'){
            foreach($head as $head_item){
                @$count += $value[$head_item]['count'];
                @$row[] = ($value[$head_item]['finalgrade'])?$value[$head_item]['finalgrade'].'%':'0%';
            }
            $table->data[$key] = array_merge(array(get_string($key, 'report_univselect', $count)), $row);
        }elseif($key == 'math_summary' || $key == 'composite' || $key == 'rw_summary'){
            foreach($head as $head_item){
                @$finalgrade = ($value[$head_item]['sumgrades'])?$value[$head_item]['sumgrades']:0;
                //$grademax = ($value[$head_item]['max'])?$value[$head_item]['max']:0;
                $row[] = $finalgrade;
            }
            $table->data[$key] = array_merge(array(get_string($key, 'report_univselect')), $row);
        }else{
            foreach($head as $head_item){
                @$count += $value[$head_item]['count'];
                @$finalgrade = ($value[$head_item]['finalgrade'])?$value[$head_item]['finalgrade']:0;
                //@$grademax = ($value[$head_item]['grademax'])?$value[$head_item]['grademax']:0;
                //$row[] = $finalgrade . '/' . $grademax;
                $row[] = $finalgrade;
            }
            $table->data[$key] = array_merge(array(get_string($key, 'report_univselect', $count)), $row);
        }
    }
    $table->data = array_values($table->data);

    return html_writer::table($table);
}

function univselect_grade_report_table_act($userid,$courseid){
    global $DB;

    $pt_tags = explode(',', get_config('report_univselect', 'practise_test_tags_act'));
    list($insql, $inparams) = $DB->get_in_or_equal($pt_tags, SQL_PARAMS_NAMED);

    $records = $DB->get_records_sql("
        SELECT
          ti.id,
          GROUP_CONCAT(t.rawname) as tags,
          q.name,
          q.sumgrades as max_grade,
          IF(q.grademethod = 1,MAX(qa.sumgrades),AVG(qa.sumgrades)) as sumgrades
        
        FROM mdl_tag t
          LEFT JOIN mdl_tag_instance ti ON ti.component='core' AND ti.itemtype='course_modules' AND ti.tagid=t.id
          LEFT JOIN mdl_course_modules cm ON cm.id=ti.itemid
          LEFT JOIN mdl_quiz q ON q.id=cm.instance
          LEFT JOIN mdl_quiz_attempts qa ON qa.quiz=cm.instance AND qa.userid=:userid
        WHERE cm.id IS NOT NULL AND cm.course=:courseid AND (t.rawname $insql OR t.rawname LIKE '%Math%' OR t.rawname LIKE '%English%' OR t.rawname LIKE '%Reading%' OR t.rawname LIKE '%Science%')
        GROUP BY cm.id, qa.quiz, qa.userid
        ", array_merge(array('courseid'=>$courseid,'userid'=>$userid), $inparams));

    $user_fields = $DB->get_records_sql("
        SELECT
          uif.shortname,
          uid.data
        FROM mdl_user_info_field uif
          LEFT JOIN mdl_user_info_data uid ON uid.fieldid=uif.id
        WHERE uif.shortname IN ('readingscore','englishscore','mathscore','sciencescore') AND uid.userid=:userid", array('userid'=>$userid));

    $data = array('english'=>array(),'math'=>array(),'reading'=>array(),'science'=>array(),'overall'=>array());
    $scales = array();
    //$scales = univselect_get_scaled_scores_act();
    //$head = array('SCORE TRACKER');
    $head = array();
    $data_overall = array();
    foreach($records as $record){
        $tags = explode(',', $record->tags);

        $first_key = '';
        $second_key = '';
        foreach($tags as $tag){
            switch($tag){
                case 'Reading':
                    $first_key = 'reading';
                    break;
                case 'Math':
                    $first_key = 'math';
                    break;
                case 'English':
                    $first_key = 'english';
                    break;
                case 'Science':
                    $first_key = 'science';
                    break;
                default:
                    $second_key = $tag;
                    break;
            }
        }

        if(!isset($scales[$second_key])){
            $scales[$second_key] = univselect_get_scaled_scores($second_key);
        }

        if(empty($data[$first_key][$second_key]))
            $data[$first_key][$second_key] = array('grademax'=>0,'sumgrades'=>0);

        $record->max_grade = (int)$record->max_grade;
        $record->sumgrades = (int)$record->sumgrades;
        $data[$first_key][$second_key]['grademax'] += $scales[$second_key][$first_key][$record->max_grade];
        $data[$first_key][$second_key]['sumgrades'] += $scales[$second_key][$first_key][$record->sumgrades];
        @$data_overall[$second_key] += $scales[$second_key][$first_key][$record->sumgrades];

        $head[$second_key] = $second_key;
    }
    ksort($head);

    $table = new html_table();
    $table->head = array_merge(array('SCORE TRACKER','Original Score'),$head);

    foreach($data as $key=>$value){
        switch($key){
            case 'math':
                @$row = array($user_fields['mathscore']->data);
                break;
            case 'english':
                @$row = array($user_fields['englishscore']->data);
                break;
            case 'science':
                @$row = array($user_fields['sciencescore']->data);
                break;
            case 'reading':
                @$row = array($user_fields['readingscore']->data);
                break;
            default:
                $row = array('-');
                break;
        }

        if($key == 'overall'){
            foreach($head as $head_item){
                @$finalgrade = ($data_overall[$head_item])?round($data_overall[$head_item]/4,2):0;
                $row[] = $finalgrade;
            }
        }else{
            foreach($head as $head_item){
                @$finalgrade = ($value[$head_item]['sumgrades'])?$value[$head_item]['sumgrades']:0;
                //@$grademax = ($value[$head_item]['grademax'])?$value[$head_item]['grademax']:0;
                $row[] = $finalgrade;
            }
        }
        $table->data[$key] = array_merge(array(get_string('key_'.$key, 'report_univselect')), $row);

    }
    $table->data = array_values($table->data);

    return html_writer::table($table);
}

function univselect_get_scaled_scores($pt_tag_name){
    global $DB;
    $tag = $DB->get_record('tag', array('rawname'=>$pt_tag_name));
    $records = $DB->get_records('report_univselect_scales', array('practice_test_id'=>$tag->id));
    $scales = array('reading'=>array(0=>0),'writing'=>array(0=>0),'math'=>array(0=>0),'english'=>array(0=>0),'science'=>array(0=>0));
    foreach ($records as $record) {
        $scales[$record->quiz_type][$record->correct] = $record->score;
    }

    return $scales;
}

function univselect_get_old_scaled_scores(){
    $scales = array();

    $reading = array(
        0=>0,
        1=>100,
        2=>100,
        3=>110,
        4=>120,
        5=>130,
        6=>140,
        7=>150,
        8=>150,
        9=>160,
        10=>170,
        11=>170,
        12=>180,
        13=>190,
        14=>190,
        15=>200,
        16=>200,
        17=>210,
        18=>210,
        19=>220,
        20=>220,
        21=>230,
        22=>230,
        23=>240,
        24=>240,
        25=>250,
        26=>250,
        27=>260,
        28=>260,
        29=>270,
        30=>280,
        31=>280,
        32=>290,
        33=>290,
        34=>300,
        35=>300,
        36=>310,
        37=>310,
        38=>320,
        39=>320,
        40=>330,
        41=>330,
        42=>340,
        43=>350,
        44=>350,
        45=>360,
        46=>370,
        47=>370,
        48=>380,
        49=>380,
        50=>390,
        51=>400,
        52=>400
    );
    $writing = array(
        0=>0,
        1=>100,
        2=>100,
        3=>100,
        4=>110,
        5=>120,
        6=>130,
        7=>130,
        8=>140,
        9=>150,
        10=>160,
        11=>160,
        12=>170,
        13=>180,
        14=>190,
        15=>190,
        16=>200,
        17=>210,
        18=>210,
        19=>220,
        20=>230,
        21=>230,
        22=>240,
        23=>250,
        24=>250,
        25=>260,
        26=>260,
        27=>270,
        28=>280,
        29=>280,
        30=>290,
        31=>300,
        32=>300,
        33=>310,
        34=>320,
        35=>320,
        36=>330,
        37=>340,
        38=>340,
        39=>350,
        40=>360,
        41=>370,
        42=>380,
        43=>390,
        44=>400
    );
    $math = array(
        0=>0,
        1=>200,
        2=>200,
        3=>230,
        4=>240,
        5=>260,
        6=>280,
        7=>290,
        8=>310,
        9=>320,
        10=>330,
        11=>340,
        12=>360,
        13=>370,
        14=>380,
        15=>390,
        16=>410,
        17=>420,
        18=>430,
        19=>440,
        20=>450,
        21=>460,
        22=>470,
        23=>480,
        24=>480,
        25=>490,
        26=>500,
        27=>510,
        28=>520,
        29=>520,
        30=>530,
        31=>540,
        32=>550,
        33=>560,
        34=>560,
        35=>570,
        36=>580,
        37=>590,
        38=>600,
        39=>600,
        40=>610,
        41=>620,
        42=>630,
        43=>640,
        44=>650,
        45=>660,
        46=>670,
        47=>670,
        48=>680,
        49=>690,
        50=>700,
        51=>710,
        52=>730,
        53=>740,
        54=>750,
        55=>760,
        56=>780,
        57=>790,
        58=>800
    );
    $scales['Practice Test 1'] = array('reading'=>$reading,'writing'=>$writing,'math'=>$math);

    $reading = array(
        0=>0,
        1=>100,
        2=>100,
        3=>100,
        4=>110,
        5=>130,
        6=>140,
        7=>140,
        8=>150,
        9=>160,
        10=>170,
        11=>170,
        12=>180,
        13=>180,
        14=>190,
        15=>200,
        16=>200,
        17=>210,
        18=>210,
        19=>220,
        20=>220,
        21=>230,
        22=>230,
        23=>240,
        24=>240,
        25=>240,
        26=>250,
        27=>250,
        28=>260,
        29=>270,
        30=>270,
        31=>280,
        32=>280,
        33=>290,
        34=>290,
        35=>300,
        36=>300,
        37=>310,
        38=>310,
        39=>320,
        40=>320,
        41=>330,
        42=>330,
        43=>340,
        44=>340,
        45=>350,
        46=>350,
        47=>360,
        48=>370,
        49=>370,
        50=>380,
        51=>390,
        52=>400
    );
    $writing = array(
        0=>0,
        1=>100,
        2=>100,
        3=>110,
        4=>120,
        5=>130,
        6=>140,
        7=>140,
        8=>150,
        9=>160,
        10=>170,
        11=>170,
        12=>180,
        13=>180,
        14=>190,
        15=>200,
        16=>200,
        17=>210,
        18=>220,
        19=>220,
        20=>230,
        21=>230,
        22=>240,
        23=>250,
        24=>250,
        25=>260,
        26=>260,
        27=>270,
        28=>270,
        29=>280,
        30=>290,
        31=>290,
        32=>300,
        33=>310,
        34=>310,
        35=>320,
        36=>330,
        37=>330,
        38=>340,
        39=>350,
        40=>360,
        41=>370,
        42=>380,
        43=>390,
        44=>400
    );
    $math = array(
        0=>0,
        1=>200,
        2=>210,
        3=>230,
        4=>250,
        5=>270,
        6=>290,
        7=>300,
        8=>320,
        9=>330,
        10=>340,
        11=>360,
        12=>370,
        13=>390,
        14=>390,
        15=>400,
        16=>420,
        17=>430,
        18=>440,
        19=>450,
        20=>460,
        21=>470,
        22=>480,
        23=>490,
        24=>500,
        25=>510,
        26=>510,
        27=>520,
        28=>530,
        29=>540,
        30=>550,
        31=>560,
        32=>570,
        33=>570,
        34=>580,
        35=>590,
        36=>600,
        37=>610,
        38=>620,
        39=>630,
        40=>640,
        41=>650,
        42=>650,
        43=>660,
        44=>670,
        45=>680,
        46=>690,
        47=>690,
        48=>700,
        49=>710,
        50=>720,
        51=>730,
        52=>740,
        53=>750,
        54=>760,
        55=>770,
        56=>780,
        57=>790,
        58=>800
    );
    $scales['Practice Test 2'] = array('reading'=>$reading,'writing'=>$writing,'math'=>$math);

    $reading = array(
        0=>0,
        1=>100,
        2=>100,
        3=>110,
        4=>120,
        5=>130,
        6=>140,
        7=>150,
        8=>150,
        9=>160,
        10=>170,
        11=>170,
        12=>180,
        13=>190,
        14=>190,
        15=>200,
        16=>210,
        17=>210,
        18=>220,
        19=>220,
        20=>230,
        21=>240,
        22=>240,
        23=>250,
        24=>250,
        25=>260,
        26=>260,
        27=>270,
        28=>270,
        29=>280,
        30=>280,
        31=>290,
        32=>290,
        33=>290,
        34=>300,
        35=>300,
        36=>310,
        37=>310,
        38=>320,
        39=>320,
        40=>330,
        41=>330,
        42=>340,
        43=>340,
        44=>350,
        45=>360,
        46=>360,
        47=>370,
        48=>380,
        49=>380,
        50=>390,
        51=>400,
        52=>400
    );
    $writing = array(
        0=>0,
        1=>100,
        2=>100,
        3=>110,
        4=>120,
        5=>130,
        6=>140,
        7=>150,
        8=>150,
        9=>160,
        10=>170,
        11=>180,
        12=>190,
        13=>190,
        14=>200,
        15=>210,
        16=>220,
        17=>220,
        18=>230,
        19=>240,
        20=>240,
        21=>250,
        22=>250,
        23=>260,
        24=>260,
        25=>270,
        26=>270,
        27=>280,
        28=>290,
        29=>290,
        30=>300,
        31=>300,
        32=>310,
        33=>320,
        34=>330,
        35=>330,
        36=>340,
        37=>340,
        38=>350,
        39=>350,
        40=>360,
        41=>370,
        42=>380,
        43=>390,
        44=>400
    );
    $math = array(
        0=>0,
        1=>200,
        2=>210,
        3=>230,
        4=>250,
        5=>270,
        6=>290,
        7=>300,
        8=>320,
        9=>330,
        10=>340,
        11=>360,
        12=>370,
        13=>380,
        14=>390,
        15=>410,
        16=>420,
        17=>430,
        18=>440,
        19=>450,
        20=>460,
        21=>470,
        22=>480,
        23=>490,
        24=>500,
        25=>510,
        26=>530,
        27=>540,
        28=>550,
        29=>560,
        30=>570,
        31=>580,
        32=>580,
        33=>590,
        34=>600,
        35=>610,
        36=>620,
        37=>630,
        38=>630,
        39=>640,
        40=>650,
        41=>660,
        42=>660,
        43=>670,
        44=>680,
        45=>680,
        46=>690,
        47=>690,
        48=>700,
        49=>710,
        50=>710,
        51=>720,
        52=>730,
        53=>740,
        54=>750,
        55=>770,
        56=>780,
        57=>790,
        58=>800
    );
    $scales['Practice Test 3'] = array('reading'=>$reading,'writing'=>$writing,'math'=>$math);

    $reading = array(
        0=>0,
        1=>100,
        2=>100,
        3=>110,
        4=>120,
        5=>130,
        6=>140,
        7=>150,
        8=>150,
        9=>160,
        10=>170,
        11=>170,
        12=>180,
        13=>190,
        14=>190,
        15=>200,
        16=>210,
        17=>210,
        18=>220,
        19=>220,
        20=>230,
        21=>240,
        22=>240,
        23=>250,
        24=>250,
        25=>260,
        26=>260,
        27=>270,
        28=>270,
        29=>280,
        30=>280,
        31=>290,
        32=>290,
        33=>290,
        34=>300,
        35=>300,
        36=>310,
        37=>310,
        38=>320,
        39=>320,
        40=>330,
        41=>330,
        42=>340,
        43=>340,
        44=>350,
        45=>360,
        46=>360,
        47=>370,
        48=>380,
        49=>380,
        50=>390,
        51=>400,
        52=>400
    );
    $writing = array(
        0=>0,
        1=>100,
        2=>100,
        3=>110,
        4=>120,
        5=>130,
        6=>140,
        7=>150,
        8=>150,
        9=>160,
        10=>170,
        11=>180,
        12=>190,
        13=>190,
        14=>200,
        15=>210,
        16=>220,
        17=>220,
        18=>230,
        19=>240,
        20=>240,
        21=>250,
        22=>250,
        23=>260,
        24=>260,
        25=>270,
        26=>270,
        27=>280,
        28=>290,
        29=>290,
        30=>300,
        31=>300,
        32=>310,
        33=>320,
        34=>330,
        35=>330,
        36=>340,
        37=>340,
        38=>350,
        39=>350,
        40=>360,
        41=>370,
        42=>380,
        43=>390,
        44=>400
    );
    $math = array(
        0=>0,
        1=>200,
        2=>210,
        3=>230,
        4=>250,
        5=>270,
        6=>290,
        7=>300,
        8=>320,
        9=>330,
        10=>340,
        11=>360,
        12=>370,
        13=>380,
        14=>390,
        15=>410,
        16=>420,
        17=>430,
        18=>440,
        19=>450,
        20=>460,
        21=>470,
        22=>480,
        23=>490,
        24=>500,
        25=>510,
        26=>530,
        27=>540,
        28=>550,
        29=>560,
        30=>570,
        31=>580,
        32=>580,
        33=>590,
        34=>600,
        35=>610,
        36=>620,
        37=>630,
        38=>630,
        39=>640,
        40=>650,
        41=>660,
        42=>660,
        43=>670,
        44=>680,
        45=>680,
        46=>690,
        47=>690,
        48=>700,
        49=>710,
        50=>710,
        51=>720,
        52=>730,
        53=>740,
        54=>750,
        55=>770,
        56=>780,
        57=>790,
        58=>800
    );
    $scales['Practice Test 4'] = array('reading'=>$reading,'writing'=>$writing,'math'=>$math);

    $reading = array(
        0=>0,
        1=>100,
        2=>100,
        3=>100,
        4=>110,
        5=>110,
        6=>120,
        7=>130,
        8=>140,
        9=>140,
        10=>150,
        11=>160,
        12=>160,
        13=>170,
        14=>170,
        15=>180,
        16=>180,
        17=>190,
        18=>200,
        19=>200,
        20=>210,
        21=>210,
        22=>220,
        23=>230,
        24=>230,
        25=>240,
        26=>240,
        27=>250,
        28=>250,
        29=>260,
        30=>260,
        31=>270,
        32=>280,
        33=>280,
        34=>290,
        35=>290,
        36=>290,
        37=>300,
        38=>300,
        39=>310,
        40=>310,
        41=>320,
        42=>320,
        43=>330,
        44=>330,
        45=>340,
        46=>350,
        47=>350,
        48=>360,
        49=>370,
        50=>370,
        51=>390,
        52=>400
    );
    $writing = array(
        0=>0,
        1=>100,
        2=>100,
        3=>100,
        4=>110,
        5=>110,
        6=>120,
        7=>130,
        8=>140,
        9=>150,
        10=>160,
        11=>160,
        12=>170,
        13=>180,
        14=>180,
        15=>190,
        16=>200,
        17=>200,
        18=>210,
        19=>220,
        20=>230,
        21=>230,
        22=>240,
        23=>250,
        24=>250,
        25=>260,
        26=>270,
        27=>280,
        28=>280,
        29=>290,
        30=>300,
        31=>300,
        32=>310,
        33=>320,
        34=>320,
        35=>330,
        36=>340,
        37=>340,
        38=>350,
        39=>360,
        40=>370,
        41=>380,
        42=>390,
        43=>400,
        44=>400
    );
    $math = array(
        0=>0,
        1=>200,
        2=>210,
        3=>230,
        4=>250,
        5=>260,
        6=>270,
        7=>290,
        8=>300,
        9=>320,
        10=>330,
        11=>340,
        12=>360,
        13=>370,
        14=>390,
        15=>400,
        16=>410,
        17=>420,
        18=>430,
        19=>440,
        20=>450,
        21=>460,
        22=>470,
        23=>480,
        24=>490,
        25=>500,
        26=>510,
        27=>510,
        28=>520,
        29=>530,
        30=>540,
        31=>540,
        32=>550,
        33=>560,
        34=>570,
        35=>580,
        36=>590,
        37=>600,
        38=>600,
        39=>610,
        40=>620,
        41=>630,
        42=>640,
        43=>650,
        44=>660,
        45=>660,
        46=>670,
        47=>680,
        48=>690,
        49=>700,
        50=>710,
        51=>710,
        52=>720,
        53=>730,
        54=>750,
        55=>760,
        56=>770,
        57=>790,
        58=>800
    );
    $scales['Practice Test 5'] = array('reading'=>$reading,'writing'=>$writing,'math'=>$math);

    $reading = array(
        0=>0,
        1=>100,
        2=>100,
        3=>100,
        4=>110,
        5=>120,
        6=>130,
        7=>140,
        8=>150,
        9=>150,
        10=>160,
        11=>170,
        12=>170,
        13=>180,
        14=>180,
        15=>190,
        16=>190,
        17=>200,
        18=>200,
        19=>210,
        20=>210,
        21=>220,
        22=>220,
        23=>230,
        24=>230,
        25=>240,
        26=>240,
        27=>250,
        28=>250,
        29=>260,
        30=>260,
        31=>270,
        32=>270,
        33=>280,
        34=>280,
        35=>290,
        36=>290,
        37=>300,
        38=>300,
        39=>310,
        40=>310,
        41=>320,
        42=>330,
        43=>330,
        44=>340,
        45=>350,
        46=>360,
        47=>370,
        48=>370,
        49=>380,
        50=>390,
        51=>400,
        52=>400
    );
    $writing = array(
        0=>0,
        1=>100,
        2=>100,
        3=>110,
        4=>110,
        5=>120,
        6=>130,
        7=>140,
        8=>150,
        9=>160,
        10=>160,
        11=>170,
        12=>180,
        13=>180,
        14=>190,
        15=>200,
        16=>200,
        17=>210,
        18=>220,
        19=>230,
        20=>230,
        21=>240,
        22=>250,
        23=>250,
        24=>260,
        25=>270,
        26=>270,
        27=>280,
        28=>280,
        29=>290,
        30=>300,
        31=>300,
        32=>310,
        33=>310,
        34=>320,
        35=>330,
        36=>340,
        37=>340,
        38=>350,
        39=>360,
        40=>360,
        41=>380,
        42=>390,
        43=>390,
        44=>400
    );
    $math = array(
        0=>0,
        1=>200,
        2=>200,
        3=>210,
        4=>250,
        5=>260,
        6=>280,
        7=>290,
        8=>310,
        9=>320,
        10=>330,
        11=>340,
        12=>350,
        13=>360,
        14=>380,
        15=>390,
        16=>400,
        17=>410,
        18=>420,
        19=>430,
        20=>440,
        21=>450,
        22=>460,
        23=>470,
        24=>490,
        25=>500,
        26=>510,
        27=>510,
        28=>520,
        29=>530,
        30=>530,
        31=>540,
        32=>550,
        33=>560,
        34=>570,
        35=>580,
        36=>590,
        37=>590,
        38=>600,
        39=>610,
        40=>620,
        41=>630,
        42=>640,
        43=>650,
        44=>660,
        45=>670,
        46=>670,
        47=>680,
        48=>690,
        49=>700,
        50=>710,
        51=>720,
        52=>730,
        53=>740,
        54=>760,
        55=>770,
        56=>780,
        57=>800,
        58=>800
    );
    $scales['Practice Test 6'] = array('reading'=>$reading,'writing'=>$writing,'math'=>$math);

    $reading = array(
        0=>0,
        1=>100,
        2=>100,
        3=>100,
        4=>110,
        5=>120,
        6=>130,
        7=>140,
        8=>150,
        9=>150,
        10=>160,
        11=>170,
        12=>170,
        13=>180,
        14=>180,
        15=>190,
        16=>200,
        17=>200,
        18=>210,
        19=>210,
        20=>220,
        21=>220,
        22=>230,
        23=>230,
        24=>240,
        25=>240,
        26=>250,
        27=>250,
        28=>260,
        29=>260,
        30=>270,
        31=>270,
        32=>280,
        33=>280,
        34=>290,
        35=>290,
        36=>300,
        37=>300,
        38=>310,
        39=>310,
        40=>320,
        41=>320,
        42=>330,
        43=>340,
        44=>350,
        45=>350,
        46=>360,
        47=>370,
        48=>370,
        49=>380,
        50=>390,
        51=>390,
        52=>400
    );
    $writing = array(
        0=>0,
        1=>100,
        2=>100,
        3=>100,
        4=>110,
        5=>120,
        6=>120,
        7=>130,
        8=>140,
        9=>150,
        10=>150,
        11=>160,
        12=>170,
        13=>180,
        14=>180,
        15=>190,
        16=>190,
        17=>200,
        18=>210,
        19=>220,
        20=>220,
        21=>230,
        22=>240,
        23=>250,
        24=>250,
        25=>260,
        26=>260,
        27=>270,
        28=>280,
        29=>290,
        30=>290,
        31=>300,
        32=>310,
        33=>310,
        34=>320,
        35=>320,
        36=>330,
        37=>340,
        38=>340,
        39=>350,
        40=>360,
        41=>360,
        42=>370,
        43=>390,
        44=>400
    );
    $math = array(
        0=>0,
        1=>200,
        2=>210,
        3=>230,
        4=>250,
        5=>260,
        6=>280,
        7=>290,
        8=>310,
        9=>320,
        10=>330,
        11=>330,
        12=>350,
        13=>360,
        14=>380,
        15=>390,
        16=>400,
        17=>420,
        18=>430,
        19=>430,
        20=>440,
        21=>450,
        22=>460,
        23=>470,
        24=>480,
        25=>490,
        26=>500,
        27=>510,
        28=>510,
        29=>520,
        30=>530,
        31=>530,
        32=>540,
        33=>550,
        34=>550,
        35=>560,
        36=>570,
        37=>580,
        38=>590,
        39=>590,
        40=>600,
        41=>610,
        42=>620,
        43=>630,
        44=>640,
        45=>650,
        46=>660,
        47=>670,
        48=>680,
        49=>680,
        50=>690,
        51=>700,
        52=>720,
        53=>730,
        54=>740,
        55=>760,
        56=>770,
        57=>790,
        58=>800
    );
    $scales['Practice Test 7'] = array('reading'=>$reading,'writing'=>$writing,'math'=>$math);

    $reading = array(
        0=>0,
        1=>100,
        2=>100,
        3=>100,
        4=>110,
        5=>120,
        6=>130,
        7=>140,
        8=>150,
        9=>150,
        10=>160,
        11=>170,
        12=>170,
        13=>180,
        14=>180,
        15=>190,
        16=>190,
        17=>200,
        18=>200,
        19=>210,
        20=>210,
        21=>220,
        22=>220,
        23=>230,
        24=>230,
        25=>240,
        26=>240,
        27=>250,
        28=>250,
        29=>260,
        30=>260,
        31=>270,
        32=>270,
        33=>280,
        34=>280,
        35=>290,
        36=>290,
        37=>290,
        38=>300,
        39=>300,
        40=>310,
        41=>310,
        42=>320,
        43=>330,
        44=>330,
        45=>340,
        46=>350,
        47=>350,
        48=>360,
        49=>370,
        50=>380,
        51=>390,
        52=>400
    );
    $writing = array(
        0=>0,
        1=>100,
        2=>100,
        3=>100,
        4=>110,
        5=>120,
        6=>130,
        7=>140,
        8=>140,
        9=>150,
        10=>160,
        11=>170,
        12=>170,
        13=>180,
        14=>190,
        15=>190,
        16=>200,
        17=>210,
        18=>210,
        19=>220,
        20=>220,
        21=>230,
        22=>240,
        23=>240,
        24=>250,
        25=>250,
        26=>260,
        27=>270,
        28=>270,
        29=>280,
        30=>290,
        31=>290,
        32=>300,
        33=>310,
        34=>310,
        35=>320,
        36=>330,
        37=>330,
        38=>340,
        39=>350,
        40=>360,
        41=>370,
        42=>380,
        43=>390,
        44=>400
    );
    $math = array(
        0=>0,
        1=>210,
        2=>220,
        3=>240,
        4=>250,
        5=>270,
        6=>290,
        7=>300,
        8=>320,
        9=>340,
        10=>350,
        11=>360,
        12=>380,
        13=>390,
        14=>400,
        15=>420,
        16=>430,
        17=>440,
        18=>450,
        19=>460,
        20=>470,
        21=>480,
        22=>490,
        23=>500,
        24=>510,
        25=>520,
        26=>520,
        27=>530,
        28=>540,
        29=>540,
        30=>550,
        31=>560,
        32=>570,
        33=>580,
        34=>580,
        35=>590,
        36=>600,
        37=>610,
        38=>620,
        39=>630,
        40=>640,
        41=>650,
        42=>660,
        43=>670,
        44=>680,
        45=>680,
        46=>690,
        47=>700,
        48=>710,
        49=>730,
        50=>740,
        51=>750,
        52=>770,
        53=>780,
        54=>790,
        55=>790,
        56=>800,
        57=>800,
        58=>800
    );
    $scales['Practice Test 8'] = array('reading'=>$reading,'writing'=>$writing,'math'=>$math);

    $reading = array(
        0=>0,
        1=>100,
        2=>100,
        3=>110,
        4=>120,
        5=>130,
        6=>130,
        7=>140,
        8=>150,
        9=>160,
        10=>170,
        11=>170,
        12=>180,
        13=>180,
        14=>190,
        15=>190,
        16=>200,
        17=>200,
        18=>210,
        19=>220,
        20=>220,
        21=>230,
        22=>230,
        23=>240,
        24=>250,
        25=>250,
        26=>260,
        27=>260,
        28=>270,
        29=>270,
        30=>280,
        31=>280,
        32=>290,
        33=>290,
        34=>300,
        35=>300,
        36=>310,
        37=>310,
        38=>320,
        39=>320,
        40=>330,
        41=>330,
        42=>340,
        43=>340,
        44=>350,
        45=>350,
        46=>360,
        47=>360,
        48=>370,
        49=>380,
        50=>380,
        51=>390,
        52=>400
    );
    $writing = array(
        0=>0,
        1=>100,
        2=>100,
        3=>110,
        4=>120,
        5=>120,
        6=>130,
        7=>140,
        8=>150,
        9=>160,
        10=>160,
        11=>170,
        12=>180,
        13=>190,
        14=>190,
        15=>200,
        16=>210,
        17=>210,
        18=>220,
        19=>220,
        20=>230,
        21=>240,
        22=>240,
        23=>250,
        24=>260,
        25=>260,
        26=>270,
        27=>270,
        28=>280,
        29=>290,
        30=>290,
        31=>300,
        32=>310,
        33=>310,
        34=>320,
        35=>330,
        36=>330,
        37=>340,
        38=>350,
        39=>350,
        40=>360,
        41=>370,
        42=>380,
        43=>390,
        44=>400
    );
    $math = array(
        0=>0,
        1=>200,
        2=>210,
        3=>220,
        4=>230,
        5=>250,
        6=>260,
        7=>280,
        8=>290,
        9=>310,
        10=>320,
        11=>330,
        12=>340,
        13=>360,
        14=>370,
        15=>380,
        16=>390,
        17=>400,
        18=>410,
        19=>420,
        20=>440,
        21=>450,
        22=>460,
        23=>470,
        24=>480,
        25=>490,
        26=>500,
        27=>510,
        28=>520,
        29=>530,
        30=>540,
        31=>550,
        32=>550,
        33=>560,
        34=>570,
        35=>580,
        36=>590,
        37=>590,
        38=>600,
        39=>610,
        40=>620,
        41=>630,
        42=>640,
        43=>650,
        44=>660,
        45=>670,
        46=>680,
        47=>690,
        48=>700,
        49=>710,
        50=>720,
        51=>740,
        52=>750,
        53=>760,
        54=>780,
        55=>790,
        56=>790,
        57=>800,
        58=>800
    );
    $scales['Practice Test 9'] = array('reading'=>$reading,'writing'=>$writing,'math'=>$math);

    $reading = array(
        0=>0,
        1=>100,
        2=>100,
        3=>100,
        4=>110,
        5=>120,
        6=>130,
        7=>130,
        8=>140,
        9=>150,
        10=>160,
        11=>160,
        12=>170,
        13=>170,
        14=>180,
        15=>180,
        16=>190,
        17=>190,
        18=>200,
        19=>200,
        20=>210,
        21=>210,
        22=>220,
        23=>220,
        24=>230,
        25=>230,
        26=>240,
        27=>240,
        28=>250,
        29=>250,
        30=>260,
        31=>260,
        32=>270,
        33=>280,
        34=>280,
        35=>290,
        36=>290,
        37=>300,
        38=>300,
        39=>310,
        40=>310,
        41=>310,
        42=>320,
        43=>320,
        44=>330,
        45=>330,
        46=>340,
        47=>350,
        48=>360,
        49=>360,
        50=>370,
        51=>390,
        52=>400
    );
    $writing = array(
        0=>0,
        1=>100,
        2=>100,
        3=>110,
        4=>110,
        5=>120,
        6=>130,
        7=>140,
        8=>140,
        9=>150,
        10=>160,
        11=>160,
        12=>170,
        13=>170,
        14=>180,
        15=>180,
        16=>180,
        17=>190,
        18=>190,
        19=>200,
        20=>200,
        21=>210,
        22=>210,
        23=>220,
        24=>230,
        25=>230,
        26=>240,
        27=>240,
        28=>250,
        29=>250,
        30=>260,
        31=>270,
        32=>270,
        33=>280,
        34=>290,
        35=>290,
        36=>300,
        37=>310,
        38=>310,
        39=>320,
        40=>330,
        41=>340,
        42=>360,
        43=>380,
        44=>400
    );
    $math = array(
        0=>0,
        1=>200,
        2=>210,
        3=>220,
        4=>230,
        5=>250,
        6=>270,
        7=>280,
        8=>300,
        9=>310,
        10=>320,
        11=>340,
        12=>350,
        13=>360,
        14=>370,
        15=>380,
        16=>390,
        17=>400,
        18=>410,
        19=>420,
        20=>430,
        21=>440,
        22=>450,
        23=>460,
        24=>470,
        25=>480,
        26=>490,
        27=>500,
        28=>500,
        29=>510,
        30=>520,
        31=>520,
        32=>530,
        33=>540,
        34=>540,
        35=>550,
        36=>560,
        37=>570,
        38=>580,
        39=>580,
        40=>590,
        41=>600,
        42=>610,
        43=>610,
        44=>620,
        45=>630,
        46=>640,
        47=>650,
        48=>660,
        49=>670,
        50=>680,
        51=>690,
        52=>700,
        53=>710,
        54=>730,
        55=>750,
        56=>770,
        57=>790,
        58=>800
    );
    $scales['Practice Test 10'] = array('reading'=>$reading,'writing'=>$writing,'math'=>$math);

    return $scales;
}

function univselect_get_old_scaled_scores_act(){
    $scales = array();

    $reading = array(
        0=>0,
        1=>3,
        2=>4,
        3=>6,
        4=>7,
        5=>9,
        6=>10,
        7=>11,
        8=>11,
        9=>12,
        10=>13,
        11=>13,
        12=>14,
        13=>15,
        14=>15,
        15=>16,
        16=>17,
        17=>18,
        18=>18,
        19=>19,
        20=>19,
        21=>20,
        22=>21,
        23=>21,
        24=>22,
        25=>23,
        26=>23,
        27=>24,
        28=>25,
        29=>26,
        30=>26,
        31=>27,
        32=>28,
        33=>29,
        34=>30,
        35=>31,
        36=>32,
        37=>33,
        38=>34,
        39=>35,
        40=>36
    );
    $english = array(
        0=>0,
        1=>1,
        2=>2,
        3=>3,
        4=>3,
        5=>4,
        6=>5,
        7=>5,
        8=>6,
        9=>6,
        10=>7,
        11=>7,
        12=>8,
        13=>8,
        14=>9,
        15=>9,
        16=>9,
        17=>10,
        18=>10,
        19=>11,
        20=>11,
        21=>11,
        22=>12,
        23=>12,
        24=>13,
        25=>13,
        26=>14,
        27=>14,
        28=>14,
        29=>15,
        30=>15,
        31=>15,
        32=>16,
        33=>16,
        34=>16,
        35=>17,
        36=>17,
        37=>18,
        38=>18,
        39=>19,
        40=>19,
        41=>20,
        42=>20,
        43=>20,
        44=>21,
        45=>21,
        46=>22,
        47=>22,
        48=>22,
        49=>23,
        50=>23,
        51=>23,
        52=>24,
        53=>24,
        54=>24,
        55=>25,
        56=>25,
        57=>25,
        58=>26,
        59=>26,
        60=>27,
        61=>27,
        62=>28,
        63=>28,
        64=>29,
        65=>30,
        66=>31,
        67=>31,
        68=>32,
        69=>33,
        70=>34,
        71=>35,
        72=>35,
        73=>35,
        74=>36,
        75=>36
    );
    $math = array(
        0=>0,
        1=>4,
        2=>6,
        3=>7,
        4=>9,
        5=>10,
        6=>11,
        7=>12,
        8=>12,
        9=>13,
        10=>13,
        11=>13,
        12=>14,
        13=>14,
        14=>15,
        15=>15,
        16=>15,
        17=>16,
        18=>16,
        19=>16,
        20=>17,
        21=>17,
        22=>17,
        23=>17,
        24=>18,
        25=>18,
        26=>19,
        27=>19,
        28=>20,
        29=>20,
        30=>21,
        31=>22,
        32=>22,
        33=>23,
        34=>23,
        35=>24,
        36=>24,
        37=>25,
        38=>25,
        39=>25,
        40=>26,
        41=>26,
        42=>27,
        43=>27,
        44=>27,
        45=>28,
        46=>28,
        47=>29,
        48=>29,
        49=>30,
        50=>31,
        51=>33,
        52=>34,
        53=>34,
        54=>35,
        55=>35,
        56=>35,
        57=>36,
        58=>36,
        59=>36,
        60=>36
    );
    $science = array(
        0=>0,
        1=>3,
        2=>5,
        3=>7,
        4=>8,
        5=>9,
        6=>10,
        7=>11,
        8=>12,
        9=>13,
        10=>14,
        11=>15,
        12=>16,
        13=>16,
        14=>17,
        15=>18,
        16=>19,
        17=>19,
        18=>20,
        19=>20,
        20=>21,
        21=>22,
        22=>22,
        23=>23,
        24=>23,
        25=>24,
        26=>24,
        27=>25,
        28=>25,
        29=>26,
        30=>26,
        31=>27,
        32=>28,
        33=>29,
        34=>31,
        35=>32,
        36=>33,
        37=>34,
        38=>35,
        39=>36,
        40=>36
    );
    $scales['ACT PT1'] = array('reading'=>$reading,'english'=>$english,'math'=>$math, 'science'=>$science);

    $reading = array(
        0=>0,
        1=>3,
        2=>4,
        3=>6,
        4=>8,
        5=>9,
        6=>10,
        7=>11,
        8=>12,
        9=>12,
        10=>13,
        11=>13,
        12=>14,
        13=>15,
        14=>15,
        15=>16,
        16=>17,
        17=>18,
        18=>18,
        19=>19,
        20=>20,
        21=>20,
        22=>21,
        23=>22,
        24=>22,
        25=>23,
        26=>24,
        27=>24,
        28=>25,
        29=>26,
        30=>27,
        31=>28,
        32=>30,
        33=>31,
        34=>32,
        35=>32,
        36=>33,
        37=>34,
        38=>35,
        39=>36,
        40=>36
    );
    $english = array(
        0=>0,
        1=>1,
        2=>2,
        3=>3,
        4=>3,
        5=>4,
        6=>4,
        7=>5,
        8=>6,
        9=>6,
        10=>6,
        11=>7,
        12=>7,
        13=>8,
        14=>8,
        15=>9,
        16=>9,
        17=>9,
        18=>10,
        19=>10,
        20=>10,
        21=>11,
        22=>11,
        23=>11,
        24=>12,
        25=>12,
        26=>13,
        27=>13,
        28=>14,
        29=>14,
        30=>14,
        31=>15,
        32=>15,
        33=>15,
        34=>16,
        35=>16,
        36=>16,
        37=>17,
        38=>17,
        39=>17,
        40=>18,
        41=>18,
        42=>19,
        43=>19,
        44=>20,
        45=>20,
        46=>20,
        47=>21,
        48=>21,
        49=>21,
        50=>22,
        51=>22,
        52=>23,
        53=>23,
        54=>23,
        55=>24,
        56=>24,
        57=>25,
        58=>25,
        59=>26,
        60=>26,
        61=>27,
        62=>27,
        63=>28,
        64=>28,
        65=>29,
        66=>30,
        67=>31,
        68=>32,
        69=>33,
        70=>33,
        71=>34,
        72=>35,
        73=>35,
        74=>36,
        75=>36
    );
    $math = array(
        0=>0,
        1=>3,
        2=>5,
        3=>7,
        4=>8,
        5=>9,
        6=>10,
        7=>11,
        8=>11,
        9=>12,
        10=>12,
        11=>13,
        12=>14,
        13=>14,
        14=>14,
        15=>15,
        16=>15,
        17=>15,
        18=>16,
        19=>16,
        20=>16,
        21=>17,
        22=>17,
        23=>17,
        24=>18,
        25=>18,
        26=>19,
        27=>19,
        28=>20,
        29=>20,
        30=>21,
        31=>21,
        32=>22,
        33=>23,
        34=>23,
        35=>24,
        36=>24,
        37=>25,
        38=>25,
        39=>26,
        40=>26,
        41=>27,
        42=>27,
        43=>28,
        44=>28,
        45=>28,
        46=>29,
        47=>30,
        48=>30,
        49=>31,
        50=>31,
        51=>32,
        52=>32,
        53=>33,
        54=>34,
        55=>34,
        56=>35,
        57=>35,
        58=>36,
        59=>36,
        60=>36
    );
    $science = array(
        0=>0,
        1=>3,
        2=>5,
        3=>7,
        4=>8,
        5=>9,
        6=>10,
        7=>11,
        8=>12,
        9=>13,
        10=>14,
        11=>15,
        12=>16,
        13=>16,
        14=>17,
        15=>18,
        16=>18,
        17=>19,
        18=>20,
        19=>20,
        20=>21,
        21=>22,
        22=>22,
        23=>23,
        24=>23,
        25=>24,
        26=>24,
        27=>25,
        28=>25,
        29=>26,
        30=>26,
        31=>27,
        32=>28,
        33=>29,
        34=>30,
        35=>31,
        36=>33,
        37=>34,
        38=>35,
        39=>36,
        40=>36
    );
    $scales['ACT PT2'] = array('reading'=>$reading,'english'=>$english,'math'=>$math, 'science'=>$science);

    $reading = array(
        0=>0,
        1=>2,
        2=>4,
        3=>6,
        4=>7,
        5=>8,
        6=>10,
        7=>10,
        8=>11,
        9=>12,
        10=>12,
        11=>13,
        12=>13,
        13=>14,
        14=>14,
        15=>15,
        16=>15,
        17=>16,
        18=>16,
        19=>17,
        20=>17,
        21=>18,
        22=>19,
        23=>19,
        24=>20,
        25=>20,
        26=>21,
        27=>22,
        28=>22,
        29=>23,
        30=>24,
        31=>25,
        32=>26,
        33=>27,
        34=>28,
        35=>29,
        36=>30,
        37=>32,
        38=>33,
        39=>34,
        40=>36
    );
    $english = array(
        0=>0,
        1=>1,
        2=>1,
        3=>2,
        4=>3,
        5=>3,
        6=>4,
        7=>4,
        8=>5,
        9=>5,
        10=>5,
        11=>6,
        12=>6,
        13=>7,
        14=>7,
        15=>7,
        16=>8,
        17=>8,
        18=>8,
        19=>9,
        20=>9,
        21=>9,
        22=>10,
        23=>10,
        24=>11,
        25=>11,
        26=>12,
        27=>12,
        28=>13,
        29=>13,
        30=>14,
        31=>14,
        32=>15,
        33=>15,
        34=>15,
        35=>16,
        36=>16,
        37=>17,
        38=>17,
        39=>17,
        40=>18,
        41=>18,
        42=>19,
        43=>19,
        44=>20,
        45=>20,
        46=>20,
        47=>21,
        48=>21,
        49=>21,
        50=>22,
        51=>22,
        52=>22,
        53=>23,
        54=>23,
        55=>23,
        56=>24,
        57=>24,
        58=>25,
        59=>25,
        60=>25,
        61=>26,
        62=>26,
        63=>27,
        64=>27,
        65=>28,
        66=>29,
        67=>29,
        68=>30,
        69=>31,
        70=>32,
        71=>33,
        72=>34,
        73=>35,
        74=>35,
        75=>36
    );
    $math = array(
        0=>0,
        1=>4,
        2=>6,
        3=>8,
        4=>10,
        5=>11,
        6=>11,
        7=>12,
        8=>13,
        9=>13,
        10=>14,
        11=>14,
        12=>14,
        13=>14,
        14=>15,
        15=>15,
        16=>15,
        17=>16,
        18=>16,
        19=>16,
        20=>16,
        21=>17,
        22=>17,
        23=>17,
        24=>18,
        25=>18,
        26=>19,
        27=>19,
        28=>20,
        29=>20,
        30=>21,
        31=>22,
        32=>22,
        33=>23,
        34=>23,
        35=>24,
        36=>24,
        37=>25,
        38=>25,
        39=>26,
        40=>26,
        41=>27,
        42=>27,
        43=>27,
        44=>28,
        45=>28,
        46=>29,
        47=>30,
        48=>30,
        49=>31,
        50=>32,
        51=>32,
        52=>33,
        53=>33,
        54=>34,
        55=>34,
        56=>35,
        57=>35,
        58=>36,
        59=>36,
        60=>36
    );
    $science = array(
        0=>0,
        1=>3,
        2=>5,
        3=>6,
        4=>8,
        5=>9,
        6=>10,
        7=>11,
        8=>12,
        9=>13,
        10=>14,
        11=>15,
        12=>16,
        13=>17,
        14=>17,
        15=>18,
        16=>19,
        17=>19,
        18=>20,
        19=>20,
        20=>20,
        21=>21,
        22=>21,
        23=>22,
        24=>23,
        25=>23,
        26=>24,
        27=>24,
        28=>25,
        29=>25,
        30=>26,
        31=>27,
        32=>28,
        33=>29,
        34=>30,
        35=>31,
        36=>32,
        37=>33,
        38=>35,
        39=>36,
        40=>36
    );
    $scales['ACT PT3'] = array('reading'=>$reading,'english'=>$english,'math'=>$math, 'science'=>$science);

    $reading = array(
        0=>0,
        1=>2,
        2=>4,
        3=>6,
        4=>7,
        5=>8,
        6=>10,
        7=>10,
        8=>11,
        9=>12,
        10=>12,
        11=>13,
        12=>14,
        13=>14,
        14=>15,
        15=>16,
        16=>16,
        17=>17,
        18=>18,
        19=>19,
        20=>19,
        21=>20,
        22=>21,
        23=>21,
        24=>22,
        25=>23,
        26=>23,
        27=>24,
        28=>25,
        29=>26,
        30=>27,
        31=>28,
        32=>29,
        33=>30,
        34=>31,
        35=>32,
        36=>32,
        37=>33,
        38=>34,
        39=>35,
        40=>36
    );
    $english = array(
        0=>0,
        1=>1,
        2=>2,
        3=>2,
        4=>3,
        5=>3,
        6=>4,
        7=>4,
        8=>5,
        9=>5,
        10=>6,
        11=>6,
        12=>7,
        13=>7,
        14=>7,
        15=>8,
        16=>8,
        17=>8,
        18=>9,
        19=>9,
        20=>10,
        21=>10,
        22=>10,
        23=>11,
        24=>11,
        25=>12,
        26=>12,
        27=>13,
        28=>13,
        29=>14,
        30=>14,
        31=>14,
        32=>15,
        33=>15,
        34=>15,
        35=>15,
        36=>16,
        37=>16,
        38=>16,
        39=>17,
        40=>17,
        41=>18,
        42=>18,
        43=>19,
        44=>19,
        45=>20,
        46=>20,
        47=>20,
        48=>21,
        49=>21,
        50=>21,
        51=>22,
        52=>22,
        53=>23,
        54=>23,
        55=>23,
        56=>24,
        57=>24,
        58=>25,
        59=>25,
        60=>25,
        61=>26,
        62=>26,
        63=>27,
        64=>28,
        65=>28,
        66=>29,
        67=>30,
        68=>31,
        69=>32,
        70=>32,
        71=>33,
        72=>34,
        73=>35,
        74=>35,
        75=>36
    );
    $math = array(
        0=>0,
        1=>4,
        2=>6,
        3=>8,
        4=>10,
        5=>11,
        6=>11,
        7=>12,
        8=>13,
        9=>13,
        10=>13,
        11=>14,
        12=>14,
        13=>15,
        14=>15,
        15=>15,
        16=>15,
        17=>16,
        18=>16,
        19=>16,
        20=>16,
        21=>17,
        22=>17,
        23=>17,
        24=>18,
        25=>18,
        26=>18,
        27=>19,
        28=>19,
        29=>20,
        30=>21,
        31=>21,
        32=>22,
        33=>22,
        34=>23,
        35=>23,
        36=>24,
        37=>24,
        38=>25,
        39=>25,
        40=>26,
        41=>26,
        42=>26,
        43=>27,
        44=>27,
        45=>28,
        46=>28,
        47=>28,
        48=>29,
        49=>29,
        50=>30,
        51=>30,
        52=>31,
        53=>31,
        54=>32,
        55=>33,
        56=>33,
        57=>34,
        58=>35,
        59=>35,
        60=>36
    );
    $science = array(
        0=>0,
        1=>3,
        2=>5,
        3=>6,
        4=>7,
        5=>9,
        6=>9,
        7=>10,
        8=>11,
        9=>12,
        10=>13,
        11=>14,
        12=>15,
        13=>16,
        14=>17,
        15=>17,
        16=>18,
        17=>19,
        18=>19,
        19=>20,
        20=>20,
        21=>21,
        22=>22,
        23=>22,
        24=>23,
        25=>23,
        26=>24,
        27=>24,
        28=>25,
        29=>25,
        30=>26,
        31=>26,
        32=>27,
        33=>28,
        34=>29,
        35=>30,
        36=>31,
        37=>33,
        38=>34,
        39=>35,
        40=>36
    );
    $scales['ACT PT4'] = array('reading'=>$reading,'english'=>$english,'math'=>$math, 'science'=>$science);

    $reading = array(
        0=>0,
        1=>2,
        2=>4,
        3=>5,
        4=>7,
        5=>8,
        6=>9,
        7=>10,
        8=>11,
        9=>12,
        10=>12,
        11=>13,
        12=>14,
        13=>15,
        14=>15,
        15=>16,
        16=>17,
        17=>18,
        18=>18,
        19=>19,
        20=>20,
        21=>21,
        22=>21,
        23=>22,
        24=>23,
        25=>23,
        26=>24,
        27=>25,
        28=>26,
        29=>27,
        30=>28,
        31=>29,
        32=>30,
        33=>31,
        34=>32,
        35=>32,
        36=>33,
        37=>34,
        38=>35,
        39=>36,
        40=>36
    );
    $english = array(
        0=>0,
        1=>1,
        2=>2,
        3=>3,
        4=>3,
        5=>4,
        6=>5,
        7=>5,
        8=>6,
        9=>6,
        10=>7,
        11=>7,
        12=>7,
        13=>8,
        14=>8,
        15=>9,
        16=>9,
        17=>10,
        18=>10,
        19=>10,
        20=>10,
        21=>11,
        22=>11,
        23=>11,
        24=>12,
        25=>12,
        26=>13,
        27=>13,
        28=>13,
        29=>14,
        30=>14,
        31=>14,
        32=>15,
        33=>15,
        34=>15,
        35=>16,
        36=>16,
        37=>16,
        38=>17,
        39=>17,
        40=>18,
        41=>18,
        42=>19,
        43=>19,
        44=>20,
        45=>20,
        46=>20,
        47=>21,
        48=>21,
        49=>21,
        50=>22,
        51=>22,
        52=>23,
        53=>23,
        54=>23,
        55=>24,
        56=>24,
        57=>24,
        58=>25,
        59=>25,
        60=>25,
        61=>26,
        62=>27,
        63=>27,
        64=>28,
        65=>29,
        66=>30,
        67=>31,
        68=>32,
        69=>33,
        70=>34,
        71=>35,
        72=>35,
        73=>36,
        74=>36,
        75=>36
    );
    $math = array(
        0=>0,
        1=>4,
        2=>6,
        3=>8,
        4=>10,
        5=>11,
        6=>12,
        7=>12,
        8=>13,
        9=>13,
        10=>14,
        11=>14,
        12=>14,
        13=>15,
        14=>15,
        15=>15,
        16=>15,
        17=>16,
        18=>16,
        19=>16,
        20=>17,
        21=>17,
        22=>17,
        23=>17,
        24=>18,
        25=>18,
        26=>19,
        27=>19,
        28=>20,
        29=>20,
        30=>21,
        31=>22,
        32=>22,
        33=>23,
        34=>23,
        35=>24,
        36=>24,
        37=>25,
        38=>25,
        39=>26,
        40=>26,
        41=>26,
        42=>27,
        43=>27,
        44=>28,
        45=>28,
        46=>28,
        47=>29,
        48=>29,
        49=>30,
        50=>31,
        51=>31,
        52=>32,
        53=>33,
        54=>34,
        55=>34,
        56=>35,
        57=>35,
        58=>35,
        59=>36,
        60=>36
    );
    $science = array(
        0=>0,
        1=>3,
        2=>4,
        3=>6,
        4=>7,
        5=>8,
        6=>9,
        7=>10,
        8=>11,
        9=>12,
        10=>13,
        11=>13,
        12=>14,
        13=>15,
        14=>15,
        15=>16,
        16=>17,
        17=>17,
        18=>18,
        19=>19,
        20=>19,
        21=>20,
        22=>21,
        23=>21,
        24=>22,
        25=>23,
        26=>23,
        27=>24,
        28=>25,
        29=>25,
        30=>26,
        31=>27,
        32=>28,
        33=>29,
        34=>31,
        35=>32,
        36=>33,
        37=>34,
        38=>35,
        39=>36,
        40=>36
    );
    $scales['ACT PT5'] = array('reading'=>$reading,'english'=>$english,'math'=>$math, 'science'=>$science);

    $reading = array(
        0=>0,
        1=>2,
        2=>4,
        3=>6,
        4=>7,
        5=>8,
        6=>10,
        7=>10,
        8=>11,
        9=>12,
        10=>12,
        11=>13,
        12=>14,
        13=>14,
        14=>15,
        15=>16,
        16=>16,
        17=>17,
        18=>18,
        19=>19,
        20=>19,
        21=>20,
        22=>21,
        23=>21,
        24=>22,
        25=>23,
        26=>23,
        27=>24,
        28=>25,
        29=>26,
        30=>27,
        31=>28,
        32=>29,
        33=>30,
        34=>31,
        35=>32,
        36=>32,
        37=>33,
        38=>34,
        39=>35,
        40=>36
    );
    $english = array(
        0=>0,
        1=>1,
        2=>2,
        3=>2,
        4=>3,
        5=>3,
        6=>4,
        7=>4,
        8=>5,
        9=>5,
        10=>6,
        11=>6,
        12=>7,
        13=>7,
        14=>7,
        15=>8,
        16=>8,
        17=>8,
        18=>9,
        19=>9,
        20=>10,
        21=>10,
        22=>10,
        23=>11,
        24=>11,
        25=>12,
        26=>12,
        27=>13,
        28=>13,
        29=>14,
        30=>14,
        31=>14,
        32=>15,
        33=>15,
        34=>15,
        35=>15,
        36=>16,
        37=>16,
        38=>16,
        39=>17,
        40=>17,
        41=>18,
        42=>18,
        43=>19,
        44=>19,
        45=>20,
        46=>20,
        47=>20,
        48=>21,
        49=>21,
        50=>21,
        51=>22,
        52=>22,
        53=>23,
        54=>23,
        55=>23,
        56=>24,
        57=>24,
        58=>25,
        59=>25,
        60=>26,
        61=>26,
        62=>27,
        63=>28,
        64=>28,
        65=>29,
        66=>30,
        67=>31,
        68=>32,
        69=>32,
        70=>33,
        71=>34,
        72=>35,
        73=>35,
        74=>35,
        75=>36
    );
    $math = array(
        0=>0,
        1=>4,
        2=>6,
        3=>8,
        4=>10,
        5=>11,
        6=>11,
        7=>12,
        8=>13,
        9=>13,
        10=>13,
        11=>14,
        12=>14,
        13=>15,
        14=>15,
        15=>15,
        16=>15,
        17=>16,
        18=>16,
        19=>16,
        20=>16,
        21=>17,
        22=>17,
        23=>17,
        24=>18,
        25=>18,
        26=>18,
        27=>19,
        28=>19,
        29=>20,
        30=>21,
        31=>21,
        32=>22,
        33=>22,
        34=>23,
        35=>23,
        36=>24,
        37=>24,
        38=>25,
        39=>25,
        40=>26,
        41=>26,
        42=>26,
        43=>27,
        44=>27,
        45=>28,
        46=>28,
        47=>29,
        48=>29,
        49=>30,
        50=>30,
        51=>30,
        52=>31,
        53=>31,
        54=>32,
        55=>33,
        56=>33,
        57=>34,
        58=>35,
        59=>35,
        60=>36
    );
    $science = array(
        0=>0,
        1=>3,
        2=>5,
        3=>6,
        4=>7,
        5=>9,
        6=>9,
        7=>10,
        8=>11,
        9=>12,
        10=>13,
        11=>14,
        12=>15,
        13=>16,
        14=>17,
        15=>17,
        16=>18,
        17=>19,
        18=>19,
        19=>20,
        20=>20,
        21=>21,
        22=>22,
        23=>22,
        24=>23,
        25=>23,
        26=>24,
        27=>24,
        28=>25,
        29=>25,
        30=>26,
        31=>26,
        32=>27,
        33=>28,
        34=>29,
        35=>30,
        36=>31,
        37=>33,
        38=>34,
        39=>35,
        40=>36
    );
    $scales['ACT PT6'] = array('reading'=>$reading,'english'=>$english,'math'=>$math, 'science'=>$science);

    $reading = array(
        0=>0,
        1=>2,
        2=>4,
        3=>6,
        4=>7,
        5=>8,
        6=>10,
        7=>10,
        8=>11,
        9=>12,
        10=>12,
        11=>13,
        12=>13,
        13=>14,
        14=>14,
        15=>15,
        16=>15,
        17=>16,
        18=>16,
        19=>17,
        20=>17,
        21=>18,
        22=>19,
        23=>19,
        24=>20,
        25=>20,
        26=>21,
        27=>22,
        28=>22,
        29=>23,
        30=>24,
        31=>25,
        32=>26,
        33=>27,
        34=>28,
        35=>29,
        36=>30,
        37=>32,
        38=>33,
        39=>34,
        40=>36
    );
    $english = array(
        0=>0,
        1=>1,
        2=>1,
        3=>2,
        4=>3,
        5=>3,
        6=>4,
        7=>4,
        8=>5,
        9=>5,
        10=>5,
        11=>6,
        12=>6,
        13=>7,
        14=>7,
        15=>7,
        16=>8,
        17=>8,
        18=>8,
        19=>9,
        20=>9,
        21=>9,
        22=>10,
        23=>10,
        24=>11,
        25=>11,
        26=>12,
        27=>12,
        28=>13,
        29=>13,
        30=>14,
        31=>14,
        32=>15,
        33=>15,
        34=>15,
        35=>16,
        36=>16,
        37=>17,
        38=>17,
        39=>17,
        40=>18,
        41=>18,
        42=>19,
        43=>19,
        44=>20,
        45=>20,
        46=>20,
        47=>21,
        48=>21,
        49=>21,
        50=>22,
        51=>22,
        52=>22,
        53=>23,
        54=>23,
        55=>23,
        56=>24,
        57=>24,
        58=>25,
        59=>25,
        60=>25,
        61=>26,
        62=>26,
        63=>27,
        64=>27,
        65=>28,
        66=>29,
        67=>29,
        68=>30,
        69=>31,
        70=>32,
        71=>33,
        72=>34,
        73=>35,
        74=>35,
        75=>36
    );
    $math = array(
        0=>0,
        1=>4,
        2=>6,
        3=>8,
        4=>10,
        5=>11,
        6=>11,
        7=>12,
        8=>13,
        9=>13,
        10=>14,
        11=>14,
        12=>14,
        13=>14,
        14=>15,
        15=>15,
        16=>15,
        17=>16,
        18=>16,
        19=>16,
        20=>16,
        21=>17,
        22=>17,
        23=>17,
        24=>18,
        25=>18,
        26=>19,
        27=>19,
        28=>20,
        29=>20,
        30=>21,
        31=>22,
        32=>22,
        33=>23,
        34=>23,
        35=>24,
        36=>24,
        37=>25,
        38=>25,
        39=>26,
        40=>26,
        41=>27,
        42=>27,
        43=>27,
        44=>28,
        45=>28,
        46=>29,
        47=>30,
        48=>30,
        49=>31,
        50=>32,
        51=>32,
        52=>33,
        53=>33,
        54=>34,
        55=>34,
        56=>35,
        57=>35,
        58=>36,
        59=>36,
        60=>36
    );
    $science = array(
        0=>0,
        1=>3,
        2=>5,
        3=>6,
        4=>8,
        5=>9,
        6=>10,
        7=>11,
        8=>12,
        9=>13,
        10=>14,
        11=>15,
        12=>16,
        13=>17,
        14=>17,
        15=>18,
        16=>19,
        17=>19,
        18=>20,
        19=>20,
        20=>20,
        21=>21,
        22=>21,
        23=>22,
        24=>23,
        25=>23,
        26=>24,
        27=>24,
        28=>25,
        29=>25,
        30=>26,
        31=>27,
        32=>28,
        33=>29,
        34=>30,
        35=>31,
        36=>32,
        37=>33,
        38=>35,
        39=>36,
        40=>36
    );
    $scales['ACT PT7'] = array('reading'=>$reading,'english'=>$english,'math'=>$math, 'science'=>$science);

    $reading = array(
        0=>0,
        1=>2,
        2=>3,
        3=>5,
        4=>6,
        5=>7,
        6=>8,
        7=>9,
        8=>10,
        9=>11,
        10=>11,
        11=>12,
        12=>12,
        13=>13,
        14=>14,
        15=>14,
        16=>15,
        17=>15,
        18=>16,
        19=>17,
        20=>18,
        21=>18,
        22=>19,
        23=>20,
        24=>20,
        25=>21,
        26=>22,
        27=>23,
        28=>23,
        29=>24,
        30=>25,
        31=>26,
        32=>27,
        33=>28,
        34=>29,
        35=>30,
        36=>31,
        37=>32,
        38=>34,
        39=>35,
        40=>36
    );
    $english = array(
        0=>0,
        1=>1,
        2=>1,
        3=>2,
        4=>2,
        5=>3,
        6=>4,
        7=>4,
        8=>4,
        9=>5,
        10=>5,
        11=>6,
        12=>6,
        13=>6,
        14=>7,
        15=>7,
        16=>7,
        17=>8,
        18=>8,
        19=>8,
        20=>9,
        21=>9,
        22=>9,
        23=>10,
        24=>10,
        25=>11,
        26=>11,
        27=>12,
        28=>12,
        29=>13,
        30=>14,
        31=>14,
        32=>14,
        33=>15,
        34=>15,
        35=>16,
        36=>16,
        37=>16,
        38=>17,
        39=>17,
        40=>18,
        41=>18,
        42=>19,
        43=>19,
        44=>19,
        45=>20,
        46=>20,
        47=>20,
        48=>21,
        49=>21,
        50=>21,
        51=>22,
        52=>22,
        53=>23,
        54=>23,
        55=>23,
        56=>24,
        57=>24,
        58=>25,
        59=>25,
        60=>26,
        61=>26,
        62=>27,
        63=>27,
        64=>28,
        65=>28,
        66=>29,
        67=>30,
        68=>31,
        69=>32,
        70=>33,
        71=>34,
        72=>34,
        73=>35,
        74=>35,
        75=>36
    );
    $math = array(
        0=>0,
        1=>3,
        2=>5,
        3=>7,
        4=>9,
        5=>10,
        6=>11,
        7=>11,
        8=>12,
        9=>12,
        10=>13,
        11=>13,
        12=>14,
        13=>14,
        14=>14,
        15=>15,
        16=>15,
        17=>15,
        18=>15,
        19=>16,
        20=>16,
        21=>16,
        22=>16,
        23=>16,
        24=>17,
        25=>17,
        26=>17,
        27=>18,
        28=>18,
        29=>19,
        30=>19,
        31=>20,
        32=>20,
        33=>21,
        34=>22,
        35=>22,
        36=>23,
        37=>23,
        38=>24,
        39=>24,
        40=>24,
        41=>25,
        42=>25,
        43=>26,
        44=>26,
        45=>27,
        46=>27,
        47=>28,
        48=>28,
        49=>29,
        50=>30,
        51=>30,
        52=>31,
        53=>32,
        54=>33,
        55=>34,
        56=>34,
        57=>35,
        58=>35,
        59=>36,
        60=>36
    );
    $science = array(
        0=>0,
        1=>3,
        2=>4,
        3=>6,
        4=>7,
        5=>8,
        6=>9,
        7=>10,
        8=>11,
        9=>12,
        10=>13,
        11=>14,
        12=>15,
        13=>16,
        14=>17,
        15=>17,
        16=>18,
        17=>19,
        18=>19,
        19=>20,
        20=>20,
        21=>21,
        22=>21,
        23=>22,
        24=>23,
        25=>23,
        26=>24,
        27=>24,
        28=>25,
        29=>25,
        30=>26,
        31=>27,
        32=>27,
        33=>28,
        34=>29,
        35=>30,
        36=>31,
        37=>33,
        38=>34,
        39=>35,
        40=>36
    );
    $scales['ACT PT8'] = array('reading'=>$reading,'english'=>$english,'math'=>$math, 'science'=>$science);

    $reading = array(
        0=>0,
        1=>2,
        2=>4,
        3=>6,
        4=>7,
        5=>8,
        6=>10,
        7=>10,
        8=>11,
        9=>12,
        10=>12,
        11=>13,
        12=>14,
        13=>14,
        14=>15,
        15=>16,
        16=>16,
        17=>17,
        18=>18,
        19=>19,
        20=>19,
        21=>20,
        22=>21,
        23=>21,
        24=>22,
        25=>23,
        26=>23,
        27=>24,
        28=>25,
        29=>26,
        30=>27,
        31=>28,
        32=>29,
        33=>30,
        34=>31,
        35=>32,
        36=>32,
        37=>33,
        38=>34,
        39=>35,
        40=>36
    );
    $english = array(
        0=>0,
        1=>1,
        2=>2,
        3=>2,
        4=>3,
        5=>3,
        6=>4,
        7=>4,
        8=>5,
        9=>5,
        10=>6,
        11=>6,
        12=>7,
        13=>7,
        14=>7,
        15=>8,
        16=>8,
        17=>8,
        18=>9,
        19=>9,
        20=>10,
        21=>10,
        22=>10,
        23=>11,
        24=>11,
        25=>12,
        26=>12,
        27=>13,
        28=>13,
        29=>14,
        30=>14,
        31=>14,
        32=>15,
        33=>15,
        34=>15,
        35=>15,
        36=>16,
        37=>16,
        38=>16,
        39=>17,
        40=>17,
        41=>18,
        42=>18,
        43=>19,
        44=>19,
        45=>20,
        46=>20,
        47=>20,
        48=>21,
        49=>21,
        50=>21,
        51=>22,
        52=>22,
        53=>23,
        54=>23,
        55=>23,
        56=>24,
        57=>24,
        58=>25,
        59=>25,
        60=>26,
        61=>26,
        62=>27,
        63=>28,
        64=>28,
        65=>29,
        66=>30,
        67=>31,
        68=>32,
        69=>32,
        70=>33,
        71=>34,
        72=>35,
        73=>35,
        74=>35,
        75=>36
    );
    $math = array(
        0=>0,
        1=>4,
        2=>6,
        3=>8,
        4=>10,
        5=>11,
        6=>11,
        7=>12,
        8=>13,
        9=>13,
        10=>13,
        11=>14,
        12=>14,
        13=>15,
        14=>15,
        15=>15,
        16=>15,
        17=>16,
        18=>16,
        19=>16,
        20=>16,
        21=>17,
        22=>17,
        23=>17,
        24=>18,
        25=>18,
        26=>18,
        27=>19,
        28=>19,
        29=>20,
        30=>21,
        31=>21,
        32=>22,
        33=>22,
        34=>23,
        35=>23,
        36=>24,
        37=>24,
        38=>25,
        39=>25,
        40=>26,
        41=>26,
        42=>26,
        43=>27,
        44=>27,
        45=>28,
        46=>28,
        47=>28,
        48=>29,
        49=>29,
        50=>30,
        51=>30,
        52=>31,
        53=>31,
        54=>32,
        55=>33,
        56=>33,
        57=>34,
        58=>35,
        59=>35,
        60=>36
    );
    $science = array(
        0=>0,
        1=>3,
        2=>5,
        3=>6,
        4=>7,
        5=>9,
        6=>9,
        7=>10,
        8=>11,
        9=>12,
        10=>13,
        11=>14,
        12=>15,
        13=>16,
        14=>17,
        15=>17,
        16=>18,
        17=>19,
        18=>19,
        19=>20,
        20=>20,
        21=>21,
        22=>22,
        23=>22,
        24=>23,
        25=>23,
        26=>24,
        27=>24,
        28=>25,
        29=>25,
        30=>26,
        31=>26,
        32=>27,
        33=>28,
        34=>29,
        35=>30,
        36=>31,
        37=>33,
        38=>34,
        39=>35,
        40=>36
    );
    $scales['ACT PT9'] = array('reading'=>$reading,'english'=>$english,'math'=>$math, 'science'=>$science);

    return $scales;
}