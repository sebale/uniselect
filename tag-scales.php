<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 *
 * @package    report
 * @subpackage univselect
 * @copyright  2015
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require "../../config.php";
require "report-settings-form.php";

require_login();
$systemcontext   = context_system::instance();
require_capability('report/univselect:setting', $systemcontext);

$practise_test = required_param('practise_test', PARAM_INT);
$title = get_string('settingsunivselect', 'report_univselect');

$PAGE->set_context($systemcontext);
$PAGE->set_url('/report/univselect/tag-scales.php');
$PAGE->navbar->add($title);
$PAGE->set_title($title);
$PAGE->set_heading($title);
$PAGE->set_pagelayout('admin');

$form = new settings_univselect_insert_tag_scales(null,array($practise_test));

if (!$form->is_cancelled() && $data = $form->get_data()) {
    $record = new stdClass();
    $record->practice_test_id = $data->practise_test;
    foreach ($data->score as $quiz_type => $scale) {
        $record->quiz_type = $quiz_type;
        foreach ($scale as $correct => $score) {
            $record->correct = $correct;
            if($original_record = $DB->get_record('report_univselect_scales', (array) $record)){
                $original_record->score = $score;
                $DB->update_record('report_univselect_scales', $original_record);
            } elseif($score > 0) {
                $record->score = $score;
                $DB->insert_record('report_univselect_scales', $record);
            }
        }
    }
    redirect(new moodle_url('/report/univselect/report-settings.php'), get_string('scales_saved', 'report_univselect'));
}

echo $OUTPUT->header();
echo $OUTPUT->heading($title);

$form->display();

echo $OUTPUT->footer();

